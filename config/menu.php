<?php
return [
    [
        'title' => 'Bảng Điều Khiển',
        'route' => 'admin.dashboard',
        'check_active_prefix' => ['cp-admin'],
        'icon_class' => 'fas fa-tachometer-alt',
        'sub_menus' => []
    ],
    [
        'title' => 'Danh Mục',
        'route' => "admin.category.index",
        'check_active_prefix' => ['cp-admin/category'],
        'icon_class' => 'fas fa-book',
        'sub_menus' =>
            [
                [
                    'title' => 'Danh Sách',
                    'route' => "admin.category.index",
                    'check_active_prefix' => '',
                    'icon_class' => 'fa fa-list',
                    'sub_menus' => []
                ],
                [
                    'title' => 'Tạo Danh Mục',
                    'route' => "admin.category.create",
                    'check_active_prefix' => '',
                    'icon_class' => 'fa fa-plus-square-o',
                    'sub_menus' => [],
                ]
            ]
    ],
    [
        'title' => 'Tin Tức',
        'route' => "admin.new.index",
        'check_active_prefix' => ['cp-admin/news'],
        'icon_class' => 'fa fa-file-text-o',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.new.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo Tin Tức',
                'route' => "admin.new.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Sản phẩm',
        'route' => "admin.product.index",
        'check_active_prefix' => ['cp-admin/product'],
        'icon_class' => 'fas fa-box-open',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.product.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo San pham',
                'route' => "admin.product.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Thẻ',
        'route' => "admin.tag.index",
        'check_active_prefix' => ['cp-admin/tag'],
        'icon_class' => 'fas fa-tag',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.tag.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo Thẻ',
                'route' => "admin.tag.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Nhà cung cấp',
        'route' => "admin.vendor.index",
        'check_active_prefix' => ['cp-admin/vendor'],
        'icon_class' => 'fas fa-store',
        'sub_menus' => [
            [
                'title' => 'Danh sách nhà cung cấp',
                'route' => "admin.vendor.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo nhà cung cấp',
                'route' => "admin.vendor.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Khách Hàng',
        'route' => "admin.contact.index",
        'check_active_prefix' => ['cp-admin/contact'],
        'icon_class' => 'fas fa-comments',
        'sub_menus' => []
    ],
    [
        'title' => 'Thành viên',
        'route' => "admin.member.index",
        'check_active_prefix' => ['cp-admin/member'],
        'icon_class' => 'fas fa-comments',
        'sub_menus' => []
    ],
    [
        'title' => 'Quản lý thông báo',
        'route' => "admin.notification.index",
        'check_active_prefix' => ['cp-admin/notification'],
        'icon_class' => 'fa fa-bullhorn',
        'sub_menus' => [
            [
                'title' => 'Danh sách thông báo',
                'route' => "admin.notification.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo thông báo',
                'route' => "admin.notification.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Quản lý menu',
        'route' => "admin.header.index",
        'check_active_prefix' => ['cp-admin/header'],
        'icon_class' => 'fas fa-book',
        'sub_menus' =>
            [
                [
                    'title' => 'Danh Sách',
                    'route' => "admin.header.index",
                    'check_active_prefix' => '',
                    'icon_class' => 'fa fa-list',
                    'sub_menus' => []
                ],
                [
                    'title' => 'Tạo menu',
                    'route' => "admin.header.create",
                    'check_active_prefix' => '',
                    'icon_class' => 'fa fa-plus-square-o',
                    'sub_menus' => [],
                ]
            ]
    ],
    [
        'title' => 'Quản lý ngôn ngữ',
        'route' => "admin.language.index",
        'check_active_prefix' => ['cp-admin/language'],
        'icon_class' => 'fas fa-book',
        'sub_menus' =>
            [
                [
                    'title' => 'Danh Sách',
                    'route' => "admin.language.index",
                    'check_active_prefix' => '',
                    'icon_class' => 'fa fa-list',
                    'sub_menus' => []
                ],
                [
                    'title' => 'Tạo ngôn ngữ',
                    'route' => "admin.language.create",
                    'check_active_prefix' => '',
                    'icon_class' => 'fa fa-plus-square-o',
                    'sub_menus' => [],
                ],
                [
                    'title' => 'Cấu hình',
                    'route' => "language_config",
                    'check_active_prefix' => '',
                    'icon_class' => 'fa fa-plus-square-o',
                    'sub_menus' => [],
                ]
            ]
    ],
    [
        'title' => 'Cấu Hình',
        'route' => "admin.config.index",
        'check_active_prefix' => ['cp-admin/config'],
        'icon_class' => 'fas fa-cogs',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.config.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Tạo Cấu Hình',
                'route' => "admin.config.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Quản lý SEO',
        'route' => "admin.seo.index",
        'check_active_prefix' => ['cp-admin/seo'],
        'icon_class' => 'fas fa-cogs',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.seo.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm thông tin SEO',
                'route' => "admin.seo.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Người Dùng',
        'route' => "admin.cms-account.index",
        'check_active_prefix' => ['cp-admin/cms-accounts'],
        'icon_class' => 'fa fa-user',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.cms-account.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm Người Dùng',
                'route' => "admin.cms-account.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Banner',
        'route' => "admin.banner.index",
        'check_active_prefix' => ['cp-admin/banner'],
        'icon_class' => 'fas fa-image',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.banner.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm Banner',
                'route' => "admin.banner.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Gói Bán Hàng',
        'route' => "admin.package.index",
        'check_active_prefix' => ['cp-admin/package'],
        'icon_class' => 'fas fa-cubes',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.package.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm Gói',
                'route' => "admin.package.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Đối Tác',

        'route' => "admin.brand.index",
        'check_active_prefix' => ['cp-admin/brand'],
        'icon_class' => 'fas fa-handshake',
        'sub_menus' => [
            [
                'title' => 'Danh Sách',
                'route' => "admin.brand.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm Đối Tác',
                'route' => "admin.brand.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Nhóm quyền',

        'route' => "admin.permission.index",
        'check_active_prefix' => ['cp-admin/permissions', 'cp-admin/roles'],
        'icon_class' => 'fa fa-address-card',
        'sub_menus' => [
            [
                'title' => 'Danh sách quyền',
                'route' => "admin.permission.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm quyền',
                'route' => "admin.permission.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ],
            [
                'title' => 'Danh sách nhóm quyền',
                'route' => "admin.role.index",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-list',
                'submenus' => []
            ],
            [
                'title' => 'Thêm nhóm quyền',
                'route' => "admin.role.create",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ],
            [
                'title' => 'Khởi tạo lại nhóm quyền',
                'route' => "admin.role.sync",
                'check_active_prefix' => '',
                'icon_class' => 'fa fa-plus-square-o',
                'submenus' => []
            ]
        ]
    ],
    [
        'title' => 'Thông tin cá nhân',
        'route' => "admin.profile.index",
        'check_active_prefix' => [],
        'icon_class' => 'fas fa-sign-out-alt',
        'sub_menus' => []
    ],
    [
        'title' => 'Đăng xuất',
        'route' => "logout",
        'check_active_prefix' => [],
        'icon_class' => 'fas fa-sign-out-alt',
        'sub_menus' => []
    ]
];
