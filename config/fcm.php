<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => true,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA_iY7iT8:APA91bE1_oHhHHbS7EJ3zFOhLtkkol9a3d6Xgyli5ZAYonyBF3EJAUAYn0xrlud_owffT6HV54bWYmaeqG0W1szG1lovC6H1k88ABies7bLB0YTEtWuocD9G9Vf_kz8v2KbcOUzahP4n'),
        'sender_id' => env('FCM_SENDER_ID', '1091563129151'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
