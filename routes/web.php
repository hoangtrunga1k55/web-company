<?php

use App\Http\Controllers\Site\ContactController;
use App\Http\Controllers\Site\SiteController;
use App\Http\Controllers\Site\ThemeController;
use App\Models\News;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Site\BlogController;
use \App\Http\Controllers\Site\LanguageController;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function (){
//    echo 'Hello';
//});
Route::group([
    'middleware' => ['SEOable']
], function () {
    Route::get('/', [BlogController::class, 'index'])->name('site.index');
    Route::get('ve-chung-toi', [BlogController::class, 'aboutUs'])->name('site.about_us');
    Route::get('thiet-ke-app', [BlogController::class, 'designApp'])->name('site.design_app');
    Route::get('thiet-ke-web', [BlogController::class, 'designWeb'])->name('site.design_web');

    Route::get('/tin-tuc', [BlogController::class, 'getBlog'])->name('site.news');
    Route::get('/tin-tuc/tim-kiem', [BlogController::class, 'getBlogSearch'])->name('site.news.search');
    Route::get('/tin-tuc/{slug}', [BlogController::class, 'getBlogDetails'])->name('site.news.show');
    Route::get('/danh-muc/{slug}', [BlogController::class, 'getNewsByCategory'])->name('category.list');
    //language
    Route::get('/news', [BlogController::class, 'getBlog'])->name('site.news_lang');
    Route::get('/news/{slug}', [BlogController::class, 'getBlogDetails'])->name('site.news.show_lang');
    Route::get('/category/{slug}', [BlogController::class, 'getNewsByCategory'])->name('category.list_lang');
    Route::get('/tag/{slug}', [BlogController::class, 'getNewsByTag'])->name('tag.list');

    Route::get('/theme', [ThemeController::class, 'index'])->name('site.theme.list');
    Route::get('/theme/{slug}', [ThemeController::class, 'detail'])->name('site.theme.detail');
});
Route::get('/language/{language}', [SiteController::class, 'changeLanguageWeb'])->name('languageWeb');
Route::post('submit-contact', [ContactController::class, 'store'])->name('site.submit-contact');

Route::get('sitemap.xml', [SiteController::class, 'sitemap']);

Route::get('/vue', function () {
    return view('welcome');
});
