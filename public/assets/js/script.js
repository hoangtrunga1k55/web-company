$(document).ready(function () {
    $('.search_bar .search_icon').click(function (e) {
        $('.search_bar .navbar-brand').toggle();
    });
    $('.search_bar .close_icon').click(function (e) {
        $('#searchForm').removeClass('show');
        $('.search_bar .navbar-brand').toggle();
        e.stopImmediatePropagation();
        e.preventDefault();
    });
})

function submitContact(event) {
    event.preventDefault;

    $(".btn-submit").prop('disabled', true);

    let url = `{{ route('site.submit-contact') }}`;
    let data = new FormData();
    data.append('name', $('#name').val());
    data.append('phone', $('#phone').val());
    data.append('email', $('#email').val());
    data.append('address', $('#address').val());
    data.append('message', $('#message').val());

    $.ajax({
        method: "post",
        url: url,
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
            Toast.fire({
                icon: response.status,
                title: response.message
            })
        },
        error: function (response){
            console.log(response);
            Toast.fire({
                icon: 'error',
                title: response.responseJSON.message
            })
        }
    });
}
