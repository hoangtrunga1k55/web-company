@extends('master')
@section('content')
    @if($isMobile)
        <section class="AboutUsMobile container-fluid">
            @include("partials.mobile.about_us.banner")
            @include("partials.mobile.about_us.introduce")
            @include("partials.mobile.about_us.group")
            @include("partials.mobile.about_us.project")
        </section>
    @else
        <section class="AboutUsWeb banner_about">
            @include("partials.web.about_us.banner")
            @include("partials.web.about_us.introduce")
            @include("partials.web.about_us.group")
            @include("partials.web.about_us.project")
        </section>
    @endif
@endsection
