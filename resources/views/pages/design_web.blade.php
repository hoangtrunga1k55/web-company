@extends('master')
@section('content')
    @if($isMobile)
        <section class="DesignWebsiteMobile container-fluid">
            @include('partials.mobile.design_web.title')
            @include('partials.mobile.design_web.introduce')
            @include('partials.mobile.design_web.package')
            @include('partials.mobile.design_web.contact')
        </section>
    @else
        <section class="DesignWebsiteWeb container-fluid">
            @include('partials.web.design_web.introduce')
            @include('partials.web.design_web.package')
            @include('partials.web.design_web.contact')
        </section>
    @endif
@endsection
