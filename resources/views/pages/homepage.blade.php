@extends('master')
@section('content')
    @if($isMobile)
        <section class="HomeMobile container-fluid">
            @include("partials.mobile.homepage.banner")
            @include("partials.mobile.homepage.services")
            @include("partials.mobile.homepage.design_web")
            @include("partials.mobile.homepage.design_app")
            @include("partials.mobile.homepage.reason")
            @include("partials.mobile.homepage.partners")
            @include("partials.mobile.homepage.contact")
        </section>
    @else
        <div class="HomeWeb container-fluid">
            @include('partials.web.homepage.banner')
            @include('partials.web.homepage.services')
            @include('partials.web.homepage.design_web')
            @include('partials.web.homepage.design_app')
            @include('partials.web.homepage.reason')
            @include('partials.web.homepage.partners')
            @include('partials.web.homepage.contact')
        </div>
    @endif
@endsection
