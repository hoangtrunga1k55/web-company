@extends('master')
@section('content')
    @if($isMobile)
        <section class="DesignAppForMobile container-fluid">
            @include('partials.mobile.design_app.title')
            @include('partials.mobile.design_app.introduce')
            @include('partials.mobile.design_app.package')
            @include('partials.mobile.design_app.contact')
        </section>
    @else
        <section class="DesignAppForWeb container-fluid">
            @include('partials.web.design_app.introduction')
            @include('partials.web.design_app.package')
            @include('partials.web.design_app.contact')
        </section>
    @endif
@endsection
