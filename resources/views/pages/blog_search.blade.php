@extends('master')
@section('content')
    @if($isMobile)
    <section class="BlogSearchMobile container-fluid">
        @include("partials.mobile.blogs_search.blog-list")
        @include("partials.mobile.blogs_search.hot-blog")
        @include("partials.mobile.blogs_search.search")
        @include("partials.mobile.blogs_search.maybe-you-like")
    </section>
    @else
    <section class="BlogSearchWeb container-fluid">
        <div class="Content row">
            <div class="col-9 p-0">
                @include("partials.web.blogs_search.blog-list")
            </div>
            <div class="col-3">
                @include("partials.web.blogs_search.hot-blog")
                @include("partials.web.blogs_search.search")
            </div>
        </div>
        @include("partials.web.blogs.maybe-you-like")
    </section>
    @endif
@endsection
