@extends('master')
@section('content')

    @if($isMobile)
        <div class="BlogDetailMobile container-fluid">
            @include('partials.mobile.blog-detail.breadcrumb')
            @include('partials.mobile.blog-detail.blog-detail')
            @include('partials.mobile.blogs_search.hot-blog')
            @include('partials.mobile.blogs_search.search')
            @include('partials.mobile.blogs.maybe-you-like')
        </div>
    @else
        <section class="BlogDetailWeb container-fluid">
            @include('partials.web.blog-detail.breadcrumb')
            <div class="Content row">
                <div class="col-9 p-0">
                    @include('partials.web.blog-detail.blog-detail')
                </div>
                <div class="col-3">
                    @include('partials.web.blogs_search.hot-blog')
                    @include('partials.web.blogs_search.search')
                </div>
            </div>
            @include('partials.web.blogs.maybe-you-like')
        </section>
    @endif

@endsection
