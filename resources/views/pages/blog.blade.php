@extends('master')
@section('content')
    @if($isMobile)
        <section class="BlogMobile container-fluid">
            @include('partials.mobile.blogs.title')
            @include('partials.mobile.blogs.blog-list')
            @include('partials.mobile.blogs.hot-blog')
            @include('partials.mobile.blogs.maybe-you-like')
            @include('partials.mobile.blogs.other')
        </section>
    @else
        <section class="BlogWeb container-fluid">
            @include('partials.web.blogs.title')
            @include('partials.web.blogs.blog-list-grid')
            @include('partials.web.blogs.maybe-you-like')
            @include('partials.web.blogs.other')
        </section>
    @endif

@endsection
