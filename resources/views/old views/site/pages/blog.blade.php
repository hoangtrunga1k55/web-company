@extends('old views.site.layouts.index')
@section('content')
    @include('old views.site.layouts.blog-details.header')
    @include('old views.site.layouts.blog.section.page-header')
    @include('old views.site.layouts.blog.section.blog-grid')
@endsection
@if(isset($category) && !is_null($category))
    @section('title', env('app_name', "HAP Technology | "). $category->title)

    @section("meta")
        @if(isset($category->meta_title))
            <meta name="title" content="{{ $category->meta_title }}">
            <meta property="og:title" content="{{ $category->meta_title }}">
        @else
            <meta name="title" content="{{ $category->title ?? ""}}">
            <meta property="og:title" content="{{ $category->title ?? ""}}">
        @endif
        @if(isset($category->meta_description))
            <meta name="description" content="{{ $category->meta_description }}">
            <meta property="og:description" content="{{ $category->meta_description }}">
        @else
            <meta name="description" content="{{ $category->description ?? "" }}">
            <meta property="og:description" content="{{ $category->description ?? ""}}">
        @endif
        @if(isset($category->meta_thumbnail))
            <meta name="image" content="{{ $category->meta_thumbnail }}">
            <meta property="og:image" content="{{ $category->meta_thumbnail }}">
        @else
            <meta name="image" content="{{ $category->image ?? "" }}">
            <meta property="og:image" content="{{ $category->image ?? "" }}">
        @endif
    @endsection

@endif
