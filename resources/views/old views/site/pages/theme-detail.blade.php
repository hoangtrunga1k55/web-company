@extends('old views.site.layouts.index')
@section('content')
    @include('old views.site.layouts.blog-details.header')
    <div>
        <section class="page-header">
            <div class="container">
                <h2>Giao diện {{ $theme->title ?? "" }}</h2>
            </div>
        </section>
    </div>
    <section class="pt-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="blog-details__main">
                        <div class="blog-details__image">
                            <img src="{{ $theme->image }}" alt="">
                        </div><!-- /.blog-details__image -->
                    </div><!-- /.blog-details__main -->


                    {{-- Note block --}}
                    @if(isset($theme->note))
                        <div class="blog-author">
                            <div class="blog-author__content">
                                <p>{{ $theme->note }}</p>
                            </div><!-- /.blog-author__content -->
                        </div><!-- /.blog-author -->
                    @endif
                </div>
                <div class="col-lg-6">
                    <div class="sidebar">
                        <div class="sidebar__single">
                            <h3>{{ $theme->title }}</h3>
                            <br>
                            <div class="row">
                                <div class="col-12">
                                    Demo:
                                    <a href="{{ $theme->demo_web }}" target="_blank" class="mx-2"><i
                                            class="fas fa-desktop"></i></a>
                                    <a href="{{ $theme->demo_ios }}" target="_blank" class="mx-2"><i
                                            class="fab fa-app-store-ios"></i></a>
                                    <a href="{{ $theme->demo_android }}" target="_blank" class="mx-2"><i
                                            class="fab fa-google-play"></i></a>
                                </div>
                                <div class="col-12">
                                    Thời gian thực hiện: {{ $theme->completion_date ?? '1' }} ngày
                                    <br>
                                    Hotline Liên Hệ {!! $configs['phoneNumber'] !!}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-auto" style="border: 3px solid #fd632f; border-radius: 20px">
                                    <a href="" data-toggle="modal" data-target="#sendRequest">
                                        <div class="py-1 px-2" style="color: #fd632f;">Đặt ngay mẫu theme này</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="blog-details__content">
                        <h3>{{ $theme->title }}</h3>
                        {!!  $theme->content !!}
                    </div><!-- /.blog-details__content -->
                </div>
            </div><!-- /.row -->
        </div>
    </section>
@endsection
@section('modal')
    <div class="modal" tabindex="-1" role="dialog" id="sendRequest">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><h3>{!! $config_languages['contact_one_title'] !!}</h3></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="contact-form-validated contact-one__form"
                          novalidate="novalidate" id="contact" action="{{ route('site.submit-contact') }}">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" placeholder="{!! $config_languages['name_input'] !!}" name="name" id="name">
                            </div>
                            <div class="col-lg-6">
                                <input type="text" placeholder="Email" name="email" id="email">
                            </div>
                            <div class="col-lg-6">
                                <input type="text" placeholder="{!! $config_languages['phoneNumber']!!}" name="phone" id="phone">
                            </div>
                            <div class="col-lg-12">
                                <textarea placeholder="{!! $config_languages['theme_note'] ?? "Lời nhắn của bạn" !!}" name="message"
                                          id="message"></textarea>
                            </div>
                        </div><!-- /.row -->
                    </form>
                    <p class="d-xl-inline-block d-none mt-xl-2 mt-lg-2 mt-md-2 mt-sm-2">
                        {{$config_languages['contact_note']}} <br>
                        {{$config_languages['phone']}}: {!! $configs['phoneNumber'] !!}
                        Email: {!! $configs['email'] !!}
                    </p>
                </div>
                <div class="modal-footer">
                    <div class="col-auto" style="border: 3px solid #fd632f; border-radius: 20px">
                        <button class="btn" data-toggle="modal" onclick="submitContact()">
                            <div class="px-1" style="color: #fd632f;">{!! $config_languages['register_button']!!}</div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        function submitContact(){
            let url = `{{ route('site.submit-contact') }}`;

            let data = new FormData();
            data.append('name', $('#name').val());
            data.append('phone', $('#phone').val());
            data.append('email', $('#email').val());
            data.append('business_type', "-");
            data.append('message', $('#message').val());
            data.append('theme', `{{ $theme->title ?? "Unknown theme"}}`);

            $.ajax({
                method: "post",
                url: url,
                data: data,
                contentType: false,
                processData: false,
                success: function (response){
                    Toast.fire({
                        icon: response.status,
                        title: response.message
                    })
                },
                error: function (response){
                    console.log(response);
                    Toast.fire({
                        icon: 'error',
                        title: response.responseJSON.message
                    })
                }
            });
        }
    </script>
@endsection
