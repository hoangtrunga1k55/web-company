@extends('old views.site.layouts.index')
@section('content')
    @include('old views.site.layouts.index.header')
    @include('old views.site.layouts.index.section.banner-one')
    {{--  Tại sao doanh nghiệp nỗ lực nhưng vẫn bỏ lỡ cơ hội phát triển?  --}}
    @include('old views.site.layouts.index.section.service-one')
    {{--  Chúng tôi làm gì để giúp doanh nghiệp của bạn  --}}
    @include('old views.site.layouts.index.section.testimonials-one')
    {{--  Gói thiết kế App  --}}
    @include('old views.site.layouts.index.section.pricing-one')

    {{--  Giá trị HAPTECH mang lại cho khách hàng  --}}
    @include('old views.site.layouts.index.section.value')

    {{-- Đối tác --}}
    @include('old views.site.layouts.index.section.brand-one')

    @include('old views.site.layouts.index.section.contact-one')

{{--        @include('site.layouts.index.section.cta-onee')--}}
{{--        @include('site.layouts.index.section.funfact-one')--}}
{{--        @include('site.layouts.index.section.cta-two')--}}
{{--        @include('site.layouts.index.section.video-one')--}}
{{--        @include('site.layouts.index.section.faq-one')--}}
{{--        @include('site.layouts.index.section.blog-one')--}}
{{--        @include('site.layouts.index.section.cta-three')--}}
{{--        @include('site.layouts.index.section.mailchimp-one')--}}
@endsection
@section('javascript')
    <script>
        function submitContact(){
            let url = `{{ route('site.submit-contact') }}`;
            let data = new FormData();
            data.append('name', $('#name').val());
            data.append('phone', $('#phone').val());
            data.append('email', $('#email').val());
            data.append('address', $('#address').val());
            data.append('message', $('#message').val());

            $.ajax({
                method: "post",
                url: url,
                data: data,
                contentType: false,
                processData: false,
                success: function (response){
                    Toast.fire({
                        icon: response.status,
                        title: response.message
                    })
                },
                error: function (response){
                    console.log(response);
                    Toast.fire({
                        icon: 'error',
                        title: response.responseJSON.message
                    })
                }
            });
        }
    </script>
@endsection

