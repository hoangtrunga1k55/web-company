@extends('old views.site.layouts.index')
@section('content')
    @include('old views.site.layouts.blog-details.header')
    <div>
        <section class="page-header">
            <div class="container">
                <h2>Giao diện</h2>
            </div>
        </section>
    </div>
    <section class="pt-3">
        <div class="container">
            <div class="row">
                @foreach($themes as $theme)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="blog-one__single">
                            <div class="rounded">
                                <a href="{{ route('site.theme.detail', ['slug' => $theme->slug]) }}">
                                    <img class="img-thumbnail" style="width: 370px; height: 242px; object-fit: cover"  src="{{ $theme->image ?? "assets/images/blog/blog-1-1.jpg" }}" alt="">
                                </a>
                            </div><!-- /.blog-one__image -->
                            <div class="row pt-2" >
                                <div class="col-auto m-auto" style="border: 3px solid #fd632f; border-radius: 20px">
                                    <a href="{{ route('site.theme.detail', ['slug' => $theme->slug]) }}">
                                        <div class="py-1 px-2"  style="color: #fd632f;">Xem chi tiết</div>
                                    </a>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-auto m-auto">
                                    <h3>
                                        <a href="{{ route('site.theme.detail', ['slug' => $theme->slug]) }}" style="color: #fd632f">{{ $theme->title ?? "-" }}</a>
                                    </h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-auto m-auto">
                                    <p style="text-align: center">
                                        Bàn giao trong <strong>{{ $theme->completion_date ?? "1" }}</strong> ngày
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="post-pagination">
                {{ $themes->links('site.paginate') }}
            </div>
        </div>
    </section>
@endsection
