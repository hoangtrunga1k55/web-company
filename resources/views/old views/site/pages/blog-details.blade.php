@extends('old views.site.layouts.index')
@section('content')
    @include('old views.site.layouts.blog-details.header')
    @include('old views.site.layouts.blog-details.section.page-header')
    @include('old views.site.layouts.blog-details.section.blog-details')
@endsection


@section('title', env("APP_NAME", "HAP Technology"). " | ". $new->title)
@section('meta')
    @if(isset($new->meta_title))
        <meta name="title" content="{{ $new->meta_title }}">
        <meta property="og:title" content="{{ $new->meta_title }}">
    @endif
    @if(isset($new->meta_description))
        <meta name="description" content="{{ $new->meta_description }}">
        <meta property="og:description" content="{{ $new->meta_description }}">
    @endif
    @if(isset($new->meta_thumbnail))
        <meta name="image" content="{{ $new->meta_thumbnail }}">
        <meta property="og:image" content="{{ $new->meta_thumbnail }}">
    @else
        <meta name="image" content="{{ $new->image ?? null }}">
        <meta property="og:image" content="{{ $new->image ?? null }} ">
    @endif
    @if(isset($new->meta_keywords))
        <meta name="keywords" content="{{ $new->meta_keywords }}">
        <meta property="og:keywords" content="{{ $new->meta_keywords }}">
    @endif
@endsection
