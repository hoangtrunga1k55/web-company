    <style>
        .block-title.text-left.first{
            /* for chrome and safari*/
            -webkit-animation-duration: 1.2s;
            -webkit-animation-name: slidein;

            /*for firefox*/
            -moz-animation-duration: 1.2s;
            -moz-animation-name: slidein;

            /* for opera*/
            -o-animation-duration: 1.2s;
            -o-animation-name: slidein;

            /* Standard syntax*/
            animation-duration: 1.2s;
            animation-name: slidein;
        }
        h2.testimonials-one__text.second {
            /* for chrome and safari*/
            -webkit-animation-duration: 1.6s;
            -webkit-animation-name: slidein;

            /*for firefox*/
            -moz-animation-duration: 1.6s;
            -moz-animation-name: slidein;

            /* for opera*/
            -o-animation-duration: 1.6s;
            -o-animation-name: slidein;

            /* Standard syntax*/
            animation-duration: 1.6s;
            animation-name: slidein;
        }
        h2.testimonials-one__text.second {
            /* for chrome and safari*/
            -webkit-animation-duration: 2s;
            -webkit-animation-name: slidein;

            /*for firefox*/
            -moz-animation-duration: 2s;
            -moz-animation-name: slidein;

            /* for opera*/
            -o-animation-duration: 2s;
            -o-animation-name: slidein;

            /* Standard syntax*/
            animation-duration: 2s;
            animation-name: slidein;
        }

        h2.testimonials-one__text.third {
            /* for chrome and safari*/
            -webkit-animation-duration: 2.4s;
            -webkit-animation-name: slidein;

            /*for firefox*/
            -moz-animation-duration: 2.4s;
            -moz-animation-name: slidein;

            /* for opera*/
            -o-animation-duration: 2.4s;
            -o-animation-name: slidein;

            /* Standard syntax*/
            animation-duration: 2.4s;
            animation-name: slidein;
        }

        h2.testimonials-one__text.fourth {
            /* for chrome and safari*/
            -webkit-animation-duration: 2.8s;
            -webkit-animation-name: slidein;

            /*for firefox*/
            -moz-animation-duration: 2.8s;
            -moz-animation-name: slidein;

            /* for opera*/
            -o-animation-duration: 2.8s;
            -o-animation-name: slidein;

            /* Standard syntax*/
            animation-duration: 2.8s;
            animation-name: slidein;
        }

        @-webkit-keyframes slidein {
            from {
                margin-left: 100%;
                width: 100%;
            }

            to {
                margin-left: 0%;
                width: 100%;
            }
        }

        @-moz-keyframes slidein {
            from {
                margin-left: 100%;
                width: 100%;
            }

            to {
                margin-left: 0%;
                width: 100%;
            }
        }

        @-o-keyframes slidein {
            from {
                margin-left: 100%;
                width: 100%;
            }

            to {
                margin-left: 0%;
                width: 100%;
            }
        }

        @keyframes slidein {
            from {
                margin-left: 100%;
                width: 100%;
            }

            to {
                margin-left: 0%;
                width: 100%;
            }
        }
    </style>

