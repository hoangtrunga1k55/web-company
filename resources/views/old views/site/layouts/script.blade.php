<script src="{{ asset('assets/js/jquery-3.5.0.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/isotope.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/TweenMax.min.js') }}"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/js/theme.min.js') }}?v={{ time() }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    function maxHeight($__this) {
        let height = [];
        $__this.each(function (index, value) {
            height.push($(this).height());
        });
        var maxValueInArray = Math.max.apply(Math, height);
        $__this.css('height', maxValueInArray);
    }

    // maxHeight($(".sidebar__post__single"));

    $(document).ready(function () {
        maxHeight($(".col-lg-4.animated.fadeInLeft .pricing-one__single"));
        maxHeight($(".col-lg-3 .service-one__single"));
        maxHeight($("#blog-grid .new"));

        let yMousePos = 0;
        let lastScrolledTop = 0;
        let counter = document.getElementsByClassName("testimonials-one");
        let count = counter[0].offsetTop;
        let $check = 1;
        $(window).scroll(function (event) {
            if (lastScrolledTop != $(document).scrollTop()) {
                yMousePos -= lastScrolledTop;
                lastScrolledTop = $(document).scrollTop();
                yMousePos += lastScrolledTop;
            }
            if (yMousePos > count - 600) {
                if ($check == 1) {
                    $check = 0;
                    $('.block-title.text-left').addClass('first');
                    $('h2.testimonials-one__text:first').addClass('second');
                    $('h2.testimonials-one__text:eq(1)').addClass('third');
                    $('h2.testimonials-one__text:eq(2)').addClass('fourth');
                }
            }
        });
    })
    let widthRes = $('.container_custom').css("marginLeft");
    $(".category .main-nav__navigation-box")[0].style.setProperty('margin-left', widthRes, 'important');


    // set padding responsive for search mobile
    let pdSearch = $('nav.mobile-nav__container').css("padding-left");

    $(".side-menu__block-inner .search-nav__right")[0].style.setProperty('padding-left', pdSearch, 'important');
</script>
<script>
    $(document).ready(function (){
        $('.main-nav__main-navigation .main-nav__navigation-box li').removeClass('current');
        $('.main-nav__main-navigation .main-nav__navigation-box li').each(function (index) {
            var url = window.location.href;
            if ($(this).find('a').attr('href') == url) {
                $(this).addClass('current');
                $(this).siblings().removeClass('current');
            }
            $(this).on('click', function () {
                $(this).addClass('current');
                $(this).siblings().removeClass('current');
            })
        });
    });
</script>

{{--@dump($ga)--}}
{{--{!! $configs['ga'] !!}--}}
{{--{!! $configs['scripts'] !!}--}}
