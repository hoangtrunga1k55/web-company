@if(isset($new))
    <section class="blog-details">
        <div class="container_custom">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-sm-12">
                    <div class="blog-details__main">
                        <h1 class="text-dark">{{$new->title}}</h1>
                        <br>
                        <div class="metabox">
                            <i class="icon-date fa fa-calendar"
                               aria-hidden="true"></i><span>{{ $new->created_at->format('d-M-Y', 'Vi')}}</span>
                        </div>
                        <strong class="font-italic">
                            {{ $new->description }}
                        </strong>
                        <br><br>
                        <div class="blog-details__content" style="object-fit: fill">
                            <div>
                                {!! $new->content !!}
                            </div>
                        </div><!-- /.blog-details__content -->
                    </div><!-- /.blog-details__main -->
                </div><!-- /.col-lg-8 -->
                <div class="col-xl-4 col-lg-4 col-sm-12">
                    <div class="sidebar_custom">
                        @include('old views.site.layouts.blog.latest_post')
                        <div class="sidebar__single sidebar__tags">
                            <h3 class="sidebar__title">Tags</h3><!-- /.sidebar__title -->
                            <div class="tab-box">
                                @foreach($tags as $tag)
                                    <div class="sidebar__tags-list-item">
                                        <a style="border: 1px solid #666; padding: 5px 10px; border-radius: 20px;margin-bottom: 10px;margin-right: 5px;" href="{{route('tag.list',['slug'=>$tag['slug']])}}">
                                            <span>{{$tag->name}}</span>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div><!-- /.sidebar -->
                </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@else
    <h3 style="text-align: center">Không tồn tại bài viết</h3>
@endif
