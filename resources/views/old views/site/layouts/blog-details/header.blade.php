<header class="site-header-one stricky original">
    <div class="container-fluid">
        <div class="site-header-one__logo">
            <a href="/">
                <div id="logo-hap">
                    {!! empty($configs['logo'])?'<img src="'.asset("images/HAP-Tech.svg").'"alt="">':$configs['logo'] !!}
                    <span class="text-dark"><strong>Technology</strong></span>
                </div>
            </a>
            <span class="side-menu__toggler"><i class="fa fa-bars"></i></span><!-- /.side-menu__toggler -->
        </div><!-- /.site-header-one__logo -->
        <div class="main-nav__main-navigation">
            <ul class="main-nav__navigation-box">
                @foreach($headers as $header)
                    <li class="category_menu">
                        <a href="{{$header->link ??'/'}}">{{$header->name}}</a>
                    </li>
                @endforeach
            </ul><!-- /.main-nav__navigation-box -->
        </div><!-- /.main-nav__main-navigation -->
        @include('old views.site.layouts.blog.search')
        <div id="menu-locales" class="navbar__header__locales pr15">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false" class="nav-link dropdown-toggle">
                        @foreach($languages as $language)
                            @if(\Illuminate\Support\Facades\Config::get('app.locale') == $language['code'])
                                {{ $language['name']}}
                            @endif
                        @endforeach

                    </a>
                    <ul class="dropdown-menu" style="">
                        @foreach($languages as $language)
                            @if($language['code']!= 'all')
                                <li><a href="{{route('languageWeb',$language['code'])}}"
                                       class="dropdown-item">{{$language['name']}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.container-fluid -->
</header>
