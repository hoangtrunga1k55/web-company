<?php
use \Illuminate\Support\Facades\Config;
$raw_configs = [];
$final_configs = [];
    foreach ($all_configs as $config){
        if ($config['lang_type'] == 'all' || $config['lang_type'] == Config::get('app.locale')){
            array_push($raw_configs,$config);
        }
    }
foreach ($raw_configs as $config) {
        $final_configs[$config['key']] = $config['content'];
    }
?>
<footer id="site-footer" class="site-footer">
    <div class="site-footer__upper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pr-lg-4 col-md-8">
                    <div class="footer-widget footer-widget__about">
                        <p>
                           {!! (isset($final_configs['content'])?$final_configs['content']:"no data ") !!}</p>
                    </div><!-- /.footer-widget -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-6 col-md-4">
                    <div class="footer-widget__links-wrap">
                        <div class="footer-widget">
                            <h3 class="footer-widget__title">{!! $config_languages['contact_info'] !!}</h3>
                            {!! !empty($final_configs['address'])?$config_languages['address'].": ".__($final_configs['address']) : __('address').": Tầng 3, DMS Building - Bhomes, No28, Lô 31 <br>Khu DV TM Hà Trì, Hà Đông" !!}
                            {!! !empty($final_configs['phoneNumber'])?__('phone').": ".$final_configs['phoneNumber']: __('phone').": <a href='tel:0889.136.363'>".'0889.136.363'."</a>" !!}
                            {!! !empty($final_configs['email'])?"Email: ".$final_configs['email'] : "Email: <a href='mailto:sales@hap-technology.com'>".'mailto:sales@hap-technology.com'."</a>" !!}
                        </div><!-- /.footer-widget -->
                    </div><!-- /.footer-widget__links-wrap -->
                </div><!-- /.col-lg-5 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.site-footer__upper -->
    <div class="site-footer__bottom">
        <div class="container text-center">
            <p>© copyright 2020 by hap-technology.com</p>
        </div><!-- /.container -->
    </div><!-- /.site-footer__bottom -->
</footer><!-- /.site-footer -->
{!! $final_configs['ga'] !!}
{!! $final_configs['scripts'] !!}
{!! empty($final_configs['fb_chat']) ?? "" !!}

