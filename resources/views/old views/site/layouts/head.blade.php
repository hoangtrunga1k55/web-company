<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @hasSection('title')
        <title>@yield('title')</title>
    @else
        @if(isset(\Request::get('meta')->site_name))
            <title>{{ \Request::get('meta')->site_name }}</title>
        @else
            <title>{{ env('APP_NAME') }}</title>
        @endif
    @endif

    @hasSection('meta')
        @yield('meta')
    @else
        @if(isset(\Request::get('meta')->title))
            <meta name="title" content="{{ \Request::get('meta')->title }}">
            <meta property="og:title" content="{{ \Request::get('meta')->title }}">
        @endif
        @if(isset(\Request::get('meta')->description))
            <meta name="description" content="{{ \Request::get('meta')->description  }}">
            <meta property="og:description" content="{{ \Request::get('meta')->description }}">
        @endif
        @if(isset(\Request::get('meta')->image))
            <meta name="image" content="{{ \Request::get('meta')->image  }}">
            <meta property="og:image" content="{{ \Request::get('meta')->image  }}">
        @endif
        @if(isset(\Request::get('meta')->keywords))
            <meta name="keywords" content="{{ \Request::get('meta')->keywords  }}">
            <meta property="og:keywords" content="{{ \Request::get('meta')->keywords  }}">
        @endif
    @endif

    <meta name="url" content="{{ url()->current() }}">
    <meta property="og:url" content="{{ url()->current() }}">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="{!! empty($configs['favicon'])?asset("images/HAP-Tech.svg"):$configs['favicon'] !!}">
    <link rel="icon" type="image/png" sizes="32x32"
          href="{!! empty($configs['favicon'])?asset("images/HAP-Tech.svg"):$configs['favicon'] !!}">
    <link rel="icon" type="image/png" sizes="16x16"
          href="{!! empty($configs['favicon'])?asset("images/HAP-Tech.svg"):$configs['favicon'] !!}">
    <link rel="manifest" href="{{ asset('assets/images/favicons/site.webmanifest') }}">
    <!-- plugin scripts -->
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;500;600;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}?v={{time()}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.min.css')}}?v={{time()}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/apton-icons.min.css')}}">

    <!-- theme styles -->
    <link rel="stylesheet" href="{{asset('assets/css/style.min.css')}}?v={{time()}}">
{{--    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}?v={{time()}}">--}}
    <link rel="stylesheet" href="{{asset('assets/css/custom.min.css')}}?v={{time()}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.min.css')}}?v={{time()}}">


    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

