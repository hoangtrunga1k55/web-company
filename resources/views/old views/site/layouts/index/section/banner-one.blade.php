    <section id="banner-one" class="banner-one">

    <div class="banner-one__bg d-xl-block d-none" style="background-image: url(assets/images/banner.png);">
    </div><!-- /.banner-one__bg -->
        <div class="float-lg-right d-xl-block d-none">
            <img src="assets/images/banner.png" width="350" class="wow fadeInUp"
                 data-wow-duration="1500ms" alt="">
        </div><!-- /.banner-one__moc -->
    <div class="container-fluid">
{{--        <a href="https://www.youtube.com/watch?v=Kl5B6MBAntI" class="banner-one__video video-popup"><i--}}
{{--                    class="fa fa-play"></i></a><!-- /.banner-one__video -->--}}
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-one__content">
                    <h1 class="introduct-left" >
                        <h3>Mobile App</h3>
                        <h4>{{$config_languages['slogan']}}</h4>
                    </h1>
                    <a href="#contact-one" class="thm-btn banner-one__btn"><span>{{$config_languages['contact_now']}}</span></a>
                    <!-- /.thm-btn banner-one__btn -->
                    </div><!-- /.banner-one__content -->
{{--                <div class="d-lg-none d-block" style="background-image: url(assets/images/banner.png);">--}}

                </div><!-- /.col-lg-7 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
        <div class="clearfix"></div>

</section><!-- /.banner-one -->
