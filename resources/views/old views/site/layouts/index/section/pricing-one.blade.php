<section id="pricing-one" class="pricing-one">
    <div class="container">
        <div class="block-title text-center">
            <h3>{!! $config_languages['pricing_one_title']!!}</h3>
        </div><!-- /.block-title text-center -->
        <div class="tabed-content">
            <div id="month">
                <div class="row">
                    @foreach($packages as $package)
                        @if($package['status']==1)
                            <div class="col-lg-4 animated fadeInLeft">
                                <div class="pricing-one__single">
                                    <div class="pricing-one__circle"></div><!-- /.pricing-one__circle -->
                                    <div class="pricing-one__inner">
                                        <h3>{{$package['name']}}</h3>
                                        <ul class="list-unstyled pricing-one__list">
                                            @foreach(explode(",", $package['titles']) as $title)
                                                <li>{{$title}}</li>
                                            @endforeach
                                        </ul><!-- /.list-unstyled pricing-one__list -->
                                        <!-- /.thm-btn -->
                                    </div><!-- /.pricing-one__inner -->
                                    <div id="contact">
                                        <a href="#contact-one"
                                           class="thm-btn pricing-one__btn"><span>
                                                {!! $config_languages['contact_now']  !!}</span></a>
                                    </div>
                                </div><!-- /.pricing-one__single -->
                            </div><!-- /.col-lg-4 -->
                        @endif
                    @endforeach
                </div><!-- /.row -->
            </div><!-- /#month -->
        </div><!-- /.tabed-content -->

    </div><!-- /.container -->
</section>

