<section id="faq-one" class="faq-one">
    <img src="assets/images/shapes/faq-bg-1-1.png" class="faq-one__bg-shape-1" alt="">
    <div class="container">
        <div class="block-title text-center">
            <p>Frequently Asked Questions</p>
            <h3>Want to Ask Something <br> From Appton?</h3>
        </div><!-- /.block-title -->
        <div class="accrodion-grp wow fadeIn faq-accrodion animated" data-wow-duration="1500ms" data-grp-name="faq-accrodion" style="visibility: visible; animation-duration: 1500ms; animation-name: fadeIn;">
            <div class="accrodion active ">
                <div class="accrodion-inner">
                    <div class="accrodion-title">
                        <h4>Pre and post launch mobile app marketing pitfalls to avoid</h4>
                    </div>
                    <div class="accrodion-content" style="">
                        <div class="inner">
                            <p>There are many variations of passages of available but majority have alteration in some by inject humour or random words. Lorem ipsum dolor sit amet.</p>
                        </div><!-- /.inner -->
                    </div>
                </div><!-- /.accrodion-inner -->
            </div>
            <div class="accrodion  ">
                <div class="accrodion-inner">
                    <div class="accrodion-title">
                        <h4>Boostup your application traffic is just a step away</h4>
                    </div>
                    <div class="accrodion-content" style="display: none;">
                        <div class="inner">
                            <p>There are many variations of passages of available but majority have alteration in some by inject humour or random words. Lorem ipsum dolor sit amet.</p>
                        </div><!-- /.inner -->
                    </div>
                </div><!-- /.accrodion-inner -->
            </div>
            <div class="accrodion ">
                <div class="accrodion-inner">
                    <div class="accrodion-title">
                        <h4>How to update application new features</h4>
                    </div>
                    <div class="accrodion-content" style="display: none;">
                        <div class="inner">
                            <p>There are many variations of passages of available but majority have alteration in some by inject humour or random words. Lorem ipsum dolor sit amet.</p>
                        </div><!-- /.inner -->
                    </div>
                </div><!-- /.accrodion-inner -->
            </div>
            <div class="accrodion ">
                <div class="accrodion-inner">
                    <div class="accrodion-title">
                        <h4>How to connect with the support to improve app experience</h4>
                    </div>
                    <div class="accrodion-content" style="display: none;">
                        <div class="inner">
                            <p>There are many variations of passages of available but majority have alteration in some by inject humour or random words. Lorem ipsum dolor sit amet.</p>
                        </div><!-- /.inner -->
                    </div>
                </div><!-- /.accrodion-inner -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
