<section id="team-one" class="team-one">

    <img src="assets/images/shapes/team-1-bg-1-1.png" class="team-one__bg-shape-1" alt="">
    <div class="container">
        <div class="block-title text-center">
            <h3>{!! $config_languages['team_one_title'] !!}</h3>
        </div><!-- /.block-title text-center -->
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="team-one__single">
                    <div class="team-one__circle"></div><!-- /.team-one__circle -->
                    <div class="team-one__inner">
                        <div class="team-one__image">
                            <img src="assets/images/icon/piggy.png" alt="">
                        </div><!-- /.team-one__image -->
                        <p style="line-height: 20px" class="px-2">
                            {!! $config_languages['team_one_content1'] !!}
                            <br>&nbsp;
                        </p>
                    </div><!-- /.team-one__inner -->
                </div><!-- /.team-one__single -->
            </div><!-- /.col-lg-3 col-md-6 col-sm-12 -->
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="team-one__single">
                    <div class="team-one__circle"></div><!-- /.team-one__circle -->
                    <div class="team-one__inner">
                        <div class="team-one__image">
                            <img src="assets/images/icon/customize.png" alt="">
                        </div><!-- /.team-one__image -->
                        <p style="line-height: 20px" class="px-2">{!! $config_languages['team_one_content2'] !!}
                            <br>&nbsp;
                        </p>
                    </div><!-- /.team-one__inner -->
                </div><!-- /.team-one__single -->
            </div><!-- /.col-lg-3 col-md-6 col-sm-12 -->
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="team-one__single">
                    <div class="team-one__circle"></div><!-- /.team-one__circle -->
                    <div class="team-one__inner">
                        <div class="team-one__image">
                            <img src="assets/images/icon/trust.png" alt="">
                        </div><!-- /.team-one__image -->
                        <p style="line-height: 20px" class="px-2">{!! $config_languages['team_one_content3'] !!}</p>
                    </div><!-- /.team-one__inner -->
                </div><!-- /.team-one__single -->
            </div><!-- /.col-lg-3 col-md-6 col-sm-12 -->
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="team-one__single">
                    <div class="team-one__circle"></div><!-- /.team-one__circle -->
                    <div class="team-one__inner">
                        <div class="team-one__image">
                            <img src="assets/images/icon/report.png" alt="">
                        </div><!-- /.team-one__image -->
                        <p style="line-height: 20px" class="px-2">{!! $config_languages['team_one_content4'] !!}
                        </p>
                    </div>
                    <!-- /.team-one__inn
                 -->
                </div><!-- /.team-one__single -->
            </div><!-- /.col-lg-3 col-md-6 col-sm-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
