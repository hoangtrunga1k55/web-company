<section id="funfact-one" class="funfact-one">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="funfact-one__single">
                    <h3 class="counter">399</h3>
                    <p>Downloads</p>
                </div><!-- /.funfact-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-3 col-md-6">
                <div class="funfact-one__single">
                    <h3 class="counter">554</h3>
                    <p>Likes</p>
                </div><!-- /.funfact-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-3 col-md-6">
                <div class="funfact-one__single">
                    <h3 class="counter">75</h3>
                    <p>5 Star Rating</p>
                </div><!-- /.funfact-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-3 col-md-6">
                <div class="funfact-one__single">
                    <h3 class="counter">22</h3>
                    <p>Awards</p>
                </div><!-- /.funfact-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
