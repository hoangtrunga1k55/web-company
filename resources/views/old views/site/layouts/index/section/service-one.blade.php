<section id="service-one" class="service-one">
    <div class="container">
        <div class="block-title text-center">
            <h3 class="mx-5">{!! $config_languages['service_one_title'] !!}</h3>
        </div><!-- /.block-title text-center -->
        <div class="row">
            <div class="col-lg-3 col-md-6 wow fadeInLeft" data-wow-duration="1500ms">
                <div class="service-one__single">
                    <div class="service-one__inner">
                        <div class="service-one__circle"></div><!-- /.service-one__circle -->
                        <div class="service-one__icon">
                            <i class="apton-icon-computer-graphic"></i>
                        </div><!-- /.service-one__icon -->
{{--                        <h3>Easy to Edit</h3>--}}
                        <p><strong>{!! $config_languages['service_one_content1'] !!}</strong></p>

                    </div><!-- /.service-one__inner -->
                </div><!-- /.service-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-3 col-md-6 wow fadeInDown" data-wow-duration="1500ms">
                <div class="service-one__single">
                    <div class="service-one__inner">
                        <div class="service-one__circle"></div><!-- /.service-one__circle -->
                        <div class="service-one__icon">
                            <i class="apton-icon-development"></i>
                        </div><!-- /.service-one__icon -->
{{--                        <h3>Fully Secure</h3>--}}
                        <p><strong>{!! $config_languages['service_one_content2'] !!}</strong></p>
                    </div><!-- /.service-one__inner -->
                </div><!-- /.service-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1500ms">
                <div class="service-one__single">
                    <div class="service-one__inner">
                        <div class="service-one__circle"></div><!-- /.service-one__circle -->
                        <div class="service-one__icon">
                            <i class="apton-icon-development1"></i>
                        </div><!-- /.service-one__icon -->
{{--                        <h3>Manage User</h3>--}}
                        <p><strong>{!! $config_languages['service_one_content3'] !!}</strong></p>
                    </div><!-- /.service-one__inner -->
                </div><!-- /.service-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-3 col-md-6 wow fadeInRight" data-wow-duration="1500ms">
                <div class="service-one__single">
                    <div class="service-one__inner">
                        <div class="service-one__circle"></div><!-- /.service-one__circle -->
                        <div class="service-one__icon">
                            <i class="apton-icon-responsive"></i>
                        </div><!-- /.service-one__icon -->
{{--                        <h3>Free Trial</h3>--}}
                        <p><strong>{!! $config_languages['service_one_content4'] !!}</strong></p>
                    </div><!-- /.service-one__inner -->
                </div><!-- /.service-one__single -->
            </div><!-- /.col-lg-3 col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.service-one -->
