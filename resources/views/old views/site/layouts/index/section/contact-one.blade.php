<section id="contact-one" class="contact-one">
    <img src="assets/images/shapes/contact-bg-shape-1-1.png" class="contact-one__bg-shape-1" alt="">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <form class="contact-form-validated contact-one__form"
                      novalidate="novalidate" id="contact" action="{{ route('site.submit-contact') }}">
                    <div class="block-title">
                        <h3>{!! $config_languages['contact_one_title'] !!}</h3>
                    </div><!-- /.block-title -->
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="text" placeholder="{!! $config_languages['name_input'] !!}" name="name" id="name">
                        </div><!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <input type="text" placeholder="Email" name="email" id="email">
                        </div><!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <input type="text" placeholder="{!! $config_languages['major'] !!}" name="business_type" id="business_type">
                        </div><!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <input type="text" placeholder="{!! $config_languages['phoneNumber'] !!}" name="phone" id="phone">
                        </div><!-- /.col-lg-6 -->
                        <div class="col-lg-12">
                            <textarea placeholder="{!! $config_languages['service_desire'] !!}" name="message" id="message"></textarea>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-lg-12 text-left mt-xl-4 mt-lg-4 mt-md-2 mt-sm-2">
                            <button type="submit" class="thm-btn contact-one__btn">
                                <span>{!! $config_languages['register_button'] !!}</span>
                            </button>
                            <!-- /.thm-btn contact-one__btn -->
                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->
                </form>
                <p class="d-xl-inline-block d-none mt-xl-2 mt-lg-2 mt-md-2 mt-sm-2">
                    {{$config_languages['contact_note']}} <br>
                    {{$config_languages['phone']}}: {!! $configs['phoneNumber'] !!}
                    Email: {!! $configs['email'] !!}
                </p>
            </div>
            <div class="col-lg-5 d-flex wow fadeInRight animated" data-wow-duration="1500ms"
                 style="visibility: visible; animation-duration: 1500ms; animation-name: fadeInRight;">
                <div class="my-auto">
                    <div class="contact-one__image">
                        <img src="assets/images/register.jpg" width="568px" alt="">
                    </div><!-- /.contact-one__image -->
                </div><!-- /.my-auto -->
            </div><!-- /.col-lg-5 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
