<section id="team-one" class="team-one">

    <img src="assets/images/shapes/team-1-bg-1-1.png" class="team-one__bg-shape-1" alt="">
{{--    <img src="assets/images/shapes/team-1-bg-1-2.png" class="team-one__bg-shape-2" alt="">--}}
    <div class="container">
        <div class="block-title text-center">
            {{--            <p>Expert People</p>--}}
            <h3>Gói thiết kế App</h3>
        </div><!-- /.block-title text-center -->
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="team-one__single">
                    <div class="team-one__circle"></div><!-- /.team-one__circle -->
                    <div class="team-one__inner">
                        <h3 style="text-transform: uppercase">Gói App bán hàng</h3><hr>
                        <p>Trang chủ</p><hr>
                        <p>Quản lý popup</p><hr>
                        <p>Quản lý banner</p><hr>
                        <p>Chat tiếp thị khách hàng</p><hr>
                        <p>Giỏ hàng - Đặt hàng</p><hr>
                        <p>Tích hợp cổng thanh toán</p><hr>
                        <p>Thông báo tự động</p><hr>
                        <p>Định danh người dùng qua đăng nhập SMS</p><hr>
                        <p>Quản trị App</p><hr>
                        <p>Quản trị Web</p>
                    </div><!-- /.team-one__inner -->
                </div><!-- /.team-one__single -->
            </div><!-- /.col-lg-3 col-md-6 col-sm-12 -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="team-one__single">
                    <div class="team-one__circle"></div><!-- /.team-one__circle -->
                    <div class="team-one__inner">
                        <h3 style="text-transform: uppercase">Gói App bán hàng thương hiệu</h3><hr>
                        <p>Tất cả tính năng của gói App bán hàng</p><hr>
                        <p>Mã giới thiệu</p><hr>
                        <p>Ví tích điểm</p><hr>
                        <p>Mã giảm giá</p><hr>
                        <p>Báo cáo thống kê</p><hr>
                        <p>Đặt lịch - Booking</p><hr>
                        <p>Đồng bộ kiotviet/POS</p><hr>
                        <p>Bản đồ tìm kiếm địa điểm gần nhất</p>
                    </div><!-- /.team-one__inner -->
                </div><!-- /.team-one__single -->
            </div><!-- /.col-lg-3 col-md-6 col-sm-12 -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="team-one__single">
                    <div class="team-one__circle"></div><!-- /.team-one__circle -->
                    <div class="team-one__inner">
                        <h3 style="text-transform: uppercase">Gói thiết kế App theo yêu cầu</h3><hr>
                        <p>Tất cả các tính năng của gói App bán hàng thương hiệu</p><hr>
                        <p>Tính năng khác báo giá theo yêu cầu</p>
                    </div><!-- /.team-one__inner -->
                </div><!-- /.team-one__single -->
            </div><!-- /.col-lg-3 col-md-6 col-sm-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
