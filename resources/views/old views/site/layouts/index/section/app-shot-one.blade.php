<section id="app-shot-one" class="app-shot-one">
    <div class="container-fluid">
        <div class="block-title text-center">
            <p>Application Screenshots</p>
            <h3>Giá trị HAPTECH mang lại cho khách hàng</h3>
        </div><!-- /.block-title -->

        <div  class="app-shot-one__carousel owl-theme owl-carousel thm__owl-carousel owl-loaded owl-drag"
             data-options="{ &quot;loop&quot;: true, &quot;margin&quot;: 30, &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;autoWidth&quot;: false, &quot;autoplay&quot;: true, &quot;smartSpeed&quot;: 700, &quot;autoplayTimeout&quot;: 5000, &quot;autoplayHoverPause&quot;: true, &quot;slideBy&quot;: 5, &quot;responsive&quot;: {
                    &quot;0&quot;: { &quot;items&quot;: 1 },
                    &quot;480&quot;: { &quot;items&quot;: 2 },
                    &quot;600&quot;: { &quot;items&quot;: 3 },
                    &quot;991&quot;: { &quot;items&quot;: 4 },
                    &quot;1000&quot;: { &quot;items&quot;: 5 }
                }}">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                     style="transform: translate3d(-5742px, 0px, 0px); transition: all 3.5s ease 0s; width: 9889px;">
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-1.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-2.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-3.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-4.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-5.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-1.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-2.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-3.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-4.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-5.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-1.png" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="owl-item" style="width: 289px; margin-right: 30px;">
                        <div class="item">
                            <img src="assets/images/app-shots/app-shot-n-1-2.png" alt="Awesome Image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-nav disabled">
                <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
                <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
            </div>
{{--            <div class="owl-dots">--}}
{{--                <button role="button" class="owl-dot"><span></span></button>--}}
{{--                <button role="button" class="owl-dot"><span></span></button>--}}
{{--                <button role="button" class="owl-dot"><span></span></button>--}}
{{--            </div>--}}
        </div><!-- /.app-shot-one__carousel owl-theme owl-carousel -->
    </div><!-- /.container-fluid -->
</section>
