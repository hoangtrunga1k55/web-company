<section id="testimonials-one" class="testimonials-one">
    <div class="container">
        <div class="row ">
            <div class="col-xl-6">
                <div class="testimonials-one__thumb-carousel-wrap">
                    <div class="testimonials-one__icon d-xl-inline d-none">
                        <div class="testimonials-one__icon-inner">
                            <img src="assets/images/shapes/testi-qoute-1-1.png" alt="">
                        </div><!-- /.testimonials-one__icon-inner -->
                    </div><!-- /.testimonials-one__icon -->
                    <div
                        class="testimonials-one__thumb-carousel swiper-container-fade swiper-container-initialized swiper-container-horizontal swiper-container-free-mode swiper-container-thumbs">
                        <div class="swiper-wrapper">
                            <div class="testimonials-one__image">
                                <img src="assets/images/bb.jpg" alt="Awesome Image">
                            </div><!-- /.testimonials-one__image -->
                        </div><!-- /.swiper-slide -->
                    </div><!-- /.swiper-wrapper -->
                </div>
                <!-- /.testimonials-one__thumb-carousel -->
            </div><!-- /.col-lg-6 -->
            <div class="col-xl-6 d-flex">
                <div class="my-auto">
                    <div class="block-title text-left">
                        <h3>{!! $config_languages['testimonial_title'] !!}</h3>
                    </div><!-- /.block-title -->
                    <div class="swiper-container-initialized swiper-container-horizontal">
                        {{--                        style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px);"--}}
                        <h2 class="testimonials-one__text">{!! $config_languages['testimonial_content1'] !!}</h2>
                        <h2 class="testimonials-one__text">{!! $config_languages['testimonial_content2'] !!}</h2>
                        <h2 class="testimonials-one__text">{!! $config_languages['testimonial_content3'] !!}</h2>
                    </div><!-- /.my-auto -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
</section>
