<section id="brand-one" class="brand-one">
    <div class="container">
        <div class="block-title text-center">
            <h3 class="mx-5">{!! $config_languages['brand_one_title'] !!}</h3>
        </div><!-- /.block-title text-center -->
        <div class="brand-one__carousel owl-carousel thm__owl-carousel owl-theme owl-loaded owl-drag"
             data-options="{&quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayHoverPause&quot;: true, &quot;autoplayTimeout&quot;: 5000, &quot;items&quot;: 5, &quot;dots&quot;: false, &quot;nav&quot;: false, &quot;margin&quot;: 100, &quot;smartSpeed&quot;: 700, &quot;responsive&quot;: { &quot;0&quot;: {&quot;items&quot;: 2, &quot;margin&quot;: 30}, &quot;480&quot;: {&quot;items&quot;: 3, &quot;margin&quot;: 30}, &quot;991&quot;: {&quot;items&quot;: 4, &quot;margin&quot;: 50}, &quot;1199&quot;: {&quot;items&quot;: 5, &quot;margin&quot;: 100}}}">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                     style="transform: translate3d(-3540px, 0px, 0px); transition: all 0.7s ease 0s; width: 5664px;">
                    @foreach($brands as $brand)
                        <div class="owl-item" style="width: 136px; margin-right: 100px;">
                            <div class="item img-thumbnail" style="vertical-align: middle;">
                                <img src="{{ isset($brand['image']) ? asset($brand['image']) : null }}" alt="">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="owl-nav disabled">
                <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span>
                </button>
                <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
            </div>
            <div class="owl-dots disabled"></div>
        </div><!-- /.brand-one__carousel owl-carousel thm__owl-carousel owl-theme -->
    </div><!-- /.container -->
</section>
