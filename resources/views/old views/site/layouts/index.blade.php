<!DOCTYPE html>
<html lang="en">
@include('old views.site.layouts.head')
@include('old views.site.layouts.style')
@yield('css')
<body>
<div class="preloader">
    {!! empty($configs['logo'])?'<img src="'.asset("images/HAP-Tech.svg").'" width="75" class="preloader__image" alt="">':$configs['logo'] !!}
</div><!-- /.preloader -->

<div class="page-wrapper">
    @yield('content')
    @include('old views.site.layouts.footer')
</div><!-- /.page-wrapper -->


<a href="tel:0889136363" class="scroll-to-target scroll-to-top d-xl-none d-inline"><i class="fa fa-phone"></i></a>
<div class="side-menu__block">
    <div class="side-menu__block-overlay custom-cursor__overlay">
        <div class="cursor"></div>
        <div class="cursor-follower"></div>
    </div><!-- /.side-menu__block-overlay -->
    <div class="side-menu__block-inner ">
        <div class="side-menu__top justify-content-end">

            <a href="#" class="side-menu__toggler side-menu__close-btn"><img
                        src="{{asset('assets/images/shapes/close-1-1.png')}}" alt=""></a>
        </div><!-- /.side-menu__top -->

        @include('old views.site.layouts.blog.search')
        <nav class="mobile-nav__container">
            <!-- content is loading via js -->
        </nav>
        <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
        <div class="side-menu__content">
            <p>
                Công ty TNHH Phát Triển Công nghệ HAP GROUP
                <br>
                HAPTECH - Dịch vụ cung cấp ứng dụng
            </p>
            <p>
                Email: <a href="mailto:needhelp@apton.com">sales@hap-technology.com</a><br>
                SDT: <a href="tel:0889136363">0889.136.363</a></p>
        </div><!-- /.side-menu__content -->
    </div><!-- /.side-menu__block-inner -->
</div><!-- /.side-menu__block -->
@yield('modal')
@include('old views.site.layouts.script')
@yield('javascript')
{{--{!! empty($configs['fb_chat']) ?? "" !!}--}}
</body>

</html>
