<?php

use App\Models\News;
use App\Repositories\Contracts\NewRepositoryInterface;
use \Illuminate\Support\Facades\Session;
use \App\Helpers\DefaultLanguage;

$conditions = [];
$conditions['where'][] = ['lang_type', 'like', \Illuminate\Support\Facades\Config::get('app.locale')];
$conditions ['where'][] = ['status', 'like', News::RELEASE];
$conditions ['where'][] = ['published_at', ">=", now()];
$orderBy = array('created_at' => 'desc');

$latestpost = app(NewRepositoryInterface::class)
    ->all(array('*'), $conditions, null, $orderBy)
    ->take(3);
?>
<div class="sidebar__single sidebar__post">
    <h3 class="sidebar__title first">{!! $config_languages['latest_news'] !!}</h3><!-- /.sidebar__title -->
    <div class="sidebar__post-wrap">
        @forelse($latestpost as $post)
            <div class="sidebar__post__single latest">
                <div class="row">
                    <div class="col-xl-5 col-lg-4 col-sm-12 pr-0">
                        <div class="inner-block">
                            <a href="{{route('site.news.show',$post->slug)}}">
                                <img src="{{ $post->image }}" alt=""
                                     style="max-height: 18vh">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-8 col-sm-12 p-0">
                        <div class="sidebar__post-content">
                            <a href="{{route('site.news.show',$post['slug'])}}">
                                <div class="sidebar__title mb-0">{{$post->title}}</div>
                            </a>
                            <a class="mb-0" href="{{route('site.news.show',$post['slug'])}}">
                                {{ strlen($post->description) >= 60 ? mb_substr($post->description, 0 ,57) . "..." : $post->description }}
                            </a>
                            <p class="metabox">
                                <i class="icon-date fa fa-calendar" aria-hidden="true"></i>
                                <span>{{ $new->created_at->format('d-m-Y') }}</span>
                            </p>
                            <!-- /.sidebar__post-title -->
                        </div><!-- /.sidebar__post-content -->
                    </div>
                </div>
            </div><!-- /.sidebar__post__single -->
            <hr class="site-footer__upper_news">
        @empty
            <h3 style="text-align: center">Không tồn tại bài biết</h3>
        @endforelse

    </div><!-- /.sidebar__post-wrap -->
</div><!-- /.sidebar__single -->
