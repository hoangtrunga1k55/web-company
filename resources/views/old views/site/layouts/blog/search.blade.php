<div class="search-nav__right">
    <form class="form-inline" action="{{route('site.news')}}" method="get">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" name="title" type="text" placeholder= "{!! $config_languages['search'] !!}">
            <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
            </button>
            </div>
        </div>
    </form>
</div><!-- /.sidebar__single -->
