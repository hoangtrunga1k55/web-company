<?php

use \Illuminate\Support\Facades\DB;

$categories = DB::table('category')->where('status', 'like', 1)->limit(3)->get();
?>
<div class="sidebar__single sidebar__category">
    <h3 class="sidebar__title">Danh mục</h3><!-- /.sidebar__title -->
    <ul class="sidebar__category-list">
        @forelse($categories as $category)
            <li class="sidebar__category-list-item current">
                <a href="{{route('category.list',$category['_id'])}}">
                    {{$category['title']}}
                </a>
            </li>
        @empty
            <h1>Khong có danh mục</h1>
        @endforelse
    </ul><!-- /.sidebar__category-list -->
</div><!-- /.sidebar__single -->
