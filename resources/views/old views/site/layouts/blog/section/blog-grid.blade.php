@if($news->total() > 0)
    <section class="blog-details">
        <div class="container_custom">
            <div class="row">
                <div class="col-xl-7 col-lg-7 col-sm-12">
                    @foreach($news as $key => $new)
                        @if($key ==0)
                            <div class="blog-details__main">
                                <div class="blog-details__image">
                                    <a href="{{route('site.news.show',$new->slug)}}"> <img
                                            src="{{$new->image}}" alt=""></a>
                                </div><!-- /.blog-details__image -->
                                <div class="blog-details__content">
                                   <h3 class="mb-1"><a href="{{ route('site.news.show',$new->slug)}}">{{$new->title}}</a></h3>
                                    <div class="metabox mb-3" style="color: #737789">
                                        <i class="icon-date fa fa-calendar"
                                           aria-hidden="true"></i><span>{{$new->created_at->format('d-M-Y')}}</span>
                                    </div>
                                    <a class="mb-0" href="{{route('site.news.show',$new->slug)}}">{{$new->description}}</a>
                                </div><!-- /.blog-details__content -->
                            </div><!-- /.blog-details__main -->
                            <hr class="site-footer__upper_news">
                        @else
                            <div class="mt-lg-4">
                                <a href="{{route('site.news.show',$new->slug)}}"><h4
                                        class="sidebar__title">{{$new->title}}</h4></a>
                                <div class="sidebar__post-wrap">
                                    <div class="sidebar__post__single">
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-4 col-sm-12">
                                                <div class="inner-block">
                                                    <a href="{{route('site.news.show',$new->slug)}}">
                                                        <img src="{{ $new->image }}" alt=""></a>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-sm-12">
                                                <div class="sidebar__post-content">
                                                    <h4 class="sidebar__post-title">
                                                        <a class="mb-0" href="{{route('site.news.show',$new->slug)}}">{{$new->description}}</a>
                                                    </h4>
                                                    <div class="metabox">
                                                        <i class="icon-date fa fa-calendar"
                                                           aria-hidden="true"></i><span>{{$new->created_at->format('d-M-Y')}}</span>
                                                    </div>
                                                    <!-- /.sidebar__post-title -->
                                                </div><!-- /.sidebar__post-content -->
                                            </div>
                                        </div>
                                    </div><!-- /.sidebar__post__single -->
                                </div>
                            </div>
                            <br>
                            <hr class="site-footer__upper_news">
                        @endif
                    @endforeach
                </div><!-- /.col-lg-8 -->
                <div class="col-lg-5 col-lg-5 col-sm-12">
                    <div class="sidebar_custom">
                        @include('old views.site.layouts.blog.latest_post')
                        <div class="sidebar__single sidebar__tags">
                            <h3 class="sidebar__title">Tags</h3><!-- /.sidebar__title -->
                            <div class="tab-box">
                                @foreach($tags as $tag)
                                    <div class="sidebar__tags-list-item">
                                        <a style="border: 1px solid #666; padding: 5px 10px; border-radius: 20px;margin-bottom: 10px;margin-right: 5px;" href="{{route('tag.list',['slug'=>$tag['slug']])}}">
                                            <span>{{$tag->name}}</span>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div><!-- /.sidebar -->
                </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="post-pagination">
            {{ $news->links('site.paginate') }}
        </div><!-- /.post-pagination -->
    </section>
@else
    <h3 style="text-align: center">Không tồn tại bài viết</h3>
@endif




