{{--@extends('site.layouts.index')--}}
<div>
    <section class="page-header">
        <div class="container">
            <h2>{!! $config_languages['news'] !!}</h2>
        </div>
    </section>
</div>
<section class="category">
    <div class="container-fluild">
        <div class="main-nav__main-navigation">
            <ul class="main-nav__navigation-box">
                @if(isset($slug))
                    @forelse($categories as $category)
                        <li class="category_menu {{($category->slug == $slug) ?'current':''}}">
                            <a href="{{route('category.list',$category->slug)}}">{{$category->title}}</a>
                        </li>
                    @empty
                        <li>Không có danh mục</li>
                    @endforelse
                @else
                    @foreach($categories as $category)
                        <li class="category_menu">
                            <a href="{{route('category.list',$category->slug)}}">{{$category->title}}</a>
                        </li>
                    @endforeach
                @endif
            </ul><!-- /.main-nav__navigation-box -->
        </div>
    </div>
</section>

