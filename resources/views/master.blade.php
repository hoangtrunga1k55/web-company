<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="icon" href="/favicon.ico">
    <title>Trang chủ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000000">
    <meta name="description" content="Trang chủ">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pagination.css') }}">
</head>
@yield('css')
<body>

@include('partials.general.header')
@yield('content')
@include('partials.general.footer')
@include('partials.general.modals')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    function showResponse(response) {
        if (response.error) {
            Toast.fire({
                icon: 'error',
                title: 'Bạn cần điền vào tất cả các trường'
            })
        } else {
            Toast.fire({
                icon: 'success',
                title: 'Chúng tôi đã tiếp nhận tin nhắn của bạn'
            })
            $('#ContactNow').modal('toggle');
        }
    }

    function showError(data) {
        console.log(data)
    }
    $(document).ready(function () {
        $('.search_bar .search_icon').click(function (e) {
            $('.search_bar .navbar-brand').toggleClass('show');
        });
        $('.search_bar .close_icon').click(function (e) {
            $('#searchForm').removeClass('show');
            $('.search_bar .navbar-brand').toggleClass('show');
        });
    })

    function submitForm(event) {
        event.preventDefault;
        $(".btn-submit").prop('disabled', true);

        $.ajax({
            method: "POST",
            url: event.getAttribute('action'),
            data: new FormData(event),
            contentType: false,
            processData: false,
            success: function (data) {
                Swal.fire(
                    'Chúc mừng bạn đã hoàn tất thông tin liên hệ',
                    'Chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất',
                    'success'
                )
            },
            error: function (data) {
                Toast.fire({
                    icon: 'error',
                    title: data.responseJSON.message
                })
                $(".btn-submit").prop('disabled', false);
            }
        })
    }

</script>
@yield('javascript')
</body>

</html>
