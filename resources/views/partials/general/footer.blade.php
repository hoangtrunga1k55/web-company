@if($isMobile)
    @include('partials.mobile.footer')
@else
    @include('partials.web.footer')
@endif
