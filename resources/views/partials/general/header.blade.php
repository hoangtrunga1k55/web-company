@if($isMobile)
    @include('partials.mobile.header')
@else
    @include('partials.web.header')
@endif
