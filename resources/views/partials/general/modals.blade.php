@if($isMobile)
    @include('partials.mobile.modal_contact')
@else
    @include('partials.web.modal_contact')
@endif
