<section class="footerMobile container-fluid">
    <div class="box row">
        <div class="box__logo col-12">
            <img src="{{ asset('assets/images/home/logoFooter.svg') }}" alt="">
        </div>
        <div class="box__content">
            <div class="box__content-left col-lg-6 col-sm-12">
                <h1>
                    Công ty TNHH Phát Triển Công nghệ HAP GROUP ( HAP GROUP TECHNOLOGY
                    DEVELOPMENT COMPANY LIMITED)
                </h1>
                <ul>
                    <li>Mã số doanh nghiệp: 0109529708</li>
                    <li>Ngày cấp: 23/02/2021</li>
                    <li>Nơi cấp: Sở Kế hoạch và Đầu tư TP. Hà Nội</li>
                    <li>Đại diện: Nguyễn Triệu Phúc</li>
                    <li>Chức vụ: Giám đốc</li>
                    <li>Lĩnh vực kinh doanh: Công nghệ - Phần mềm</li>
                </ul>
            </div>
            <div class="box__content-right col-lg-6 col-sm-12">
                <h1>Thông tin liên hệ</h1>
                <ul>
                    <li>
                        Địa chỉ: Tầng 3, DBS Building - Bhomes, No28, Lô 31 Khu DV TM Hà
                        Trì, Hà Đông
                    </li>
                    <li>Điện thoại: <span>0889.136.363</span></li>
                    <li>Email: <span> sales@hap-technology.com</span></li>
                </ul>
            </div>
            <div class="box__content-bottom col-12">
                <div class="icon">
                    <img src="{{ asset('assets/images/icon/logo-facebook.png') }}" alt=""><img src="{{ asset('assets/images/icon/logo-google.png') }}" alt="">
                </div>
                <p>© copyright 2020 by hap-technology.com</p>
            </div>
        </div>
    </div>
</section>
