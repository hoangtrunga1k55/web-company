<div class="IntroduceItem row">
    <div class="col-12">
        <div class="box">
            <div class="box__introduce">
                <div class="box__introduce-title">
                    <span>Website</span> - Quản lý bán hàng
                </div>
                <div class="box__introduce-text">
                    Website bán hàng là trang Web cho phép doanh nghiệp và khách hàng có thể tiến hành việc mua bán sản
                    phẩm/ dịch vụ trực tuyến. Cụ thể, khi truy cập vào các Website này, bạn có thể xem thông tin sản
                    phẩm, tìm kiếm các sản phẩm cần mua, đặt hàng và thanh toán nhanh chóng.
                </div>
            </div>
            <div class="box__image">
                <img src="{{ asset('assets/images/home/quanlybanhang1.png') }}" alt="">
            </div>
            <div class="box__image">
                <img src="{{ asset('assets/images/home/quanlybanhang1.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="box">
            <div class="box__introduce">
                <div class="box__introduce-title">
                    <span>Website</span> - Tin tức
                </div>
                <div class="box__introduce-text">
                    Website tin tức là thiết kế một trang báo điện tử – một hình thức báo chí có khả năng tiếp cận và
                    phản hồi liên tục trong mọi thời điểm trên toàn cầu, cập nhật liên tục các thông tin nóng hổi xảy ra
                    trên khắp thế giới.
                </div>
            </div>
            <div class="box__image">
                <img src="{{ asset('assets/images/home/tintuc1.png') }}" alt="">
            </div>
            <div class="box__image">
                <img src="{{ asset('assets/images/home/tintuc2.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
