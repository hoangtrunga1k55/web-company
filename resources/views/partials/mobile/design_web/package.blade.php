<div class="PackageItem row">
    <div class="col-12 p-0">
        <div class="box">
            <table>
                <thead>
                <tr class="header">
                    <th class="header__one"></th>
                    <th>
                        GÓI STARTUP <br>
                    </th>
                    <th>
                        GÓI PRO <br>
                    </th>
                    <th class="header__four">
                        GÓI VIP <br>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr class="body">
                    <td class="body__one">Giao diện website</td>
                    <td>Theo mẫu có sẵn</td>
                    <td>Theo mẫu có sẵn</td>
                    <td class="body__four">Thiết kế độc quyền theo yêu cầu</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Dung lượng lưu trữ</td>
                    <td>10 Gb</td>
                    <td>15 Gb</td>
                    <td class="body__four">30 Gb</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Băng thông</td>
                    <td>Không giới hạn</td>
                    <td>Không giới hạn</td>
                    <td class="body__four">Không giới hạn</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Bảo mật SSL</td>
                    <td class="body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                    <td class="body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                    <td class="body__four body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                </tr>
                <tr class="body">
                    <td class="body__one">Tối ưu SEO</td>
                    <td>Chuẩn SEO 100%</td>
                    <td>Chuẩn SEO 100%</td>
                    <td class="body__four">Chuẩn SEO 100%</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Tương thích thiết bị di động</td>
                    <td class="body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                    <td class="body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                    <td class="body__four body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                </tr>
                <tr class="body">
                    <td class="body__one">Hỗ trợ bài viết</td>
                    <td>10 bài copy</td>
                    <td>15 bài copy</td>
                    <td class="body__four">30 bài copy</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Hỗ trợ tùy chỉnh</td>
                    <td>Đổi màu</td>
                    <td>Đổi màu</td>
                    <td class="body__four">Tùy chỉnh theo yêu cầu</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Email doanh nghiệp</td>
                    <td>Giảm 5%</td>
                    <td>Giảm 7%</td>
                    <td class="body__four">Giảm 10%</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Bộ nhận diện thương hiệu</td>
                    <td>Giảm 5%</td>
                    <td>Giảm 7%</td>
                    <td class="body__four">Giảm 10%</td>
                </tr>
                <tr class="body">
                    <td class="body__one">Bàn giao code</td>
                    <td class="body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                    <td class="body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                    <td class="body__four body__image">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVTSURBVHgB7Z0/bBtVHMe/7+w4vYESIkCIfzKCjUotQ5cOJIGhYiKZKBNhgAEkatSyIbWRutEKBwkGBtJOwEK9IYZCytABBoKAETB/ihCtIheQnNjOvb7fOefGsX13Tuz3O/t+Hyny5e6dFX+/fr/3e7930VPok2phKq+RnVdQh6HUrAamlPlBCjGfveIAZSiUPc+7qtAoucVKuZ/3UHEb/l+4bzGj8JK5ZRZCT4yga1rrZbd442LM9uH8V7h3NqucFXOYh9APZWi9FGVETwN0YWpq05k4o7UqQNgzWumi69WXVLFS6Xa9qwEU55XKXTYx7giEQWB6Q22u2/jQYQCJD5X7ChJyBk1XE9oMoLCzoXLfQcQfCjRATxoTdoYjZ2cDivkQ8YcGhfRqU+MWrR7QTDPVCoSh09De3F3Fm6t03OoBRvwzEKywndb7+AbQtx8SemySrzY1bxrgKHUSgl38qoJ52U47f4VgnQNOJu9QYQ0CC9Utb8ExzEBgQUEfdjwtgy8bCrM0COchsEDrKE5aF1OSAC1mORBYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYEQOYyUKAmn4EmaMnoA4chPfLNWz98Dlsoapv3q+RYtRDh5B77TKUe7B1rvHFefPzDmyQ6hDkPHGsQ3wie/x0x7mh/Q1IKZmjL3QVP0BNPwobpNKAzNOvYuLF93pe1+t/wLv+I2yQukGYwkv2+FuhbWzFfyJVPSBKfF39F/WP38DWt5/CFqnpAXHEr32wAG0p9ASkwgCK9zTo9oJifm1l0br4xFgbQBlOdv5ctPj0zTevHIytASS+n2aaiVYvuMUnxnIQHhXxCas9gGaemSefg9645Wcaw/jwVNfxxTevvaBY74tvBl5urBmwOwuh4lf9o0V4fw1u4Bs18Qk7Ici9uyMF9MV6/TM4Dx7CIIgjPvW6zQvPJkZ8wooBzvTD3S8YY3Knr4RmKXHwK5qnvowUnyZZScOKAd76n6Hfuqg8PYxu5eTdUHk5ieITdkJQ9RYapbdDm5AJUTWa3ZBpk6euRIpvs7bTL9bS0DghIE6hLIDED6toEkkXn7A6D/AHwfPhg2AcE6hNlPhkdtLFJ6xPxLRJO2vvL+zZhDgG2a5o7geWmbBvwoVnQidivtDPn+s8F1XRNOaOivgEWykiTikgO/MKJk40Q03UIB2Uk72fr2GUYH8qItbs1ZgUdT0JdZ29wF6MiyPeuIpPJKIa2hLx+k97u29ExScSU44mETf7MKFVVBth8YlkrQeYGTOZEPVo4LiITyRvQcaYUDfrs1vfdE8lKcVMUjl5vyR2Raz+iZnJfv1h27mgnDEu4hOJfzjXefyY/5igXv995HL8OKT+6Whu5B80mBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmBEDmHFoS1YILCjaDleJAWx4QJl20LgKgQeN30wP8NYgMKFWjQHZEgQWFFTJcYt/087PqxCsYpKfNdI+SEMvQbCK0nrZfw1OmLVh2k0vD8EGZffdfx6jgzsTMe29DMEOWi8Fhy0D3OLNVb3dLYThoaGX3eKNi8Hv7Ttqo36WBgcIw6Ls6vrZnSfaDLinWKko7SxQQwiDxmSbTtt+8kRHMa6ZljpzEBMGhqKoYjT1td1F12ooNdzQtadkTNg/FPMnda2r+ISKegN/6+3mfvN5CP1QpsySkpuwRpEGBJARWqmT5oYjEEIwVQWNSzsznTBiGxBQLTyQ12jMm+g1Y+7Om1P5tO7ISmspfjnflHJMqPleoVFyi5VyP+9xG9cwP5PphbWPAAAAAElFTkSuQmCC" alt="">
                    </td>
                </tr>
                <tr class="body">
                    <td class="body__one">Bảo hành web trọn đời</td>
                    <td>Khi sử dụng Hosting</td>
                    <td>Khi sử dụng Hosting</td>
                    <td class="body__four">Khi sử dụng Hosting</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
