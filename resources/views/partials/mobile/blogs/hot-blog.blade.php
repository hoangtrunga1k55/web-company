<?php
$condition['where'][] = ['slug', 'like', 'nhieu-nguoi-xem'];
$id_items = app(\App\Repositories\Contracts\CategoryRepositoryInterface::class)->all(array('_id'), $condition)->first();
$conditions['where'][] = ['category_ids', 'all', [$id_items['_id']]];
$news = app(\App\Repositories\Contracts\NewRepositoryInterface::class)->all(array('*'), $conditions)->take(4);
?>
@if($news->count() > 0)
<div class="ManyViewersItem row">
    <div class="box col-12">
        <div class="box__title">Nhiều người xem</div>
        @foreach($news as $new)
        <div class="row box__content">
            <div class="box__content-image col-6">
                <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}"
                                                                      alt=""></a>
            </div>
            <div class="box__content-text col-6">
                <a href="{{route('site.news.show',$new->slug)}}">{{$new->title}}</a>
            </div>
        </div>
        @endforeach
    </div>
</div>
@else
    <h3>Không tồn tại bài viết</h3>
@endif
