<div class="TitleItem row">
    <div class="box col-12 search_bar">
        <nav class="navbar navbar-expand navbar-light">
            <h5 class="navbar-brand show">TIN TỨC</h5>
            <div class="collapse navbar-collapse" id="navbar1">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <div class="fade collapse" id="searchForm" style="">
                            <button>
                                <img class="close_icon" src="{{asset('assets/images/icon/icon_close.png')}}" alt=""/>
                            </button>
                            <form action="{{route('site.news.search')}}" method="GET">
                            <input id="search" type="search" name="title" class="form-control border-0 bg-light" placeholder="Tìm kiếm...">
                            </form>
                        </div>
                        <div>
                            <a class="nav-link ml-auto close__from" href="#searchForm" data-target="#searchForm" data-toggle="collapse" aria-expanded="true">
                                <img class="search_icon" src="{{asset('assets/images/icon/icon_search.png')}}" alt=""/>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
