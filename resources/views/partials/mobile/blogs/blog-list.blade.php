@if($news->total() > 0)
<div class="BlogListItem row">
    @foreach($news as $key => $new)
    <div class="col-12">
        <div class="box">
            <div class="box__image">
                <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}"
                                                                      alt=""></a>
            </div>
            <div class="box__content">
                <div class="box__content-category">
                    <div class="type">PRODUCT</div>
                    <div class="dot"></div>
                    <div class="time">{{$new->created_at->format('d-M-Y')}}</div>
                </div>
                <div class="box__content-title">
                    <a href="{{route('site.news.show',$new->slug)}}">{{$new->title}}</a>
                </div>
                <div class="box__content-text">
                    <a href="{{route('site.news.show',$new->slug)}}">{!! $new->description !!}</a>
                </div>
                <div class="box__content-link">
                    <a href="{{route('site.news.show',$new->slug)}}">Xem thêm </a><img
                        src="{{ 'assets/images/icon/arrowRight.png' }}" alt="">
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@else
    <h3 style="text-align: center">Không tồn tại bài viết</h3>
@endif
<div class="post-pagination">
    {{ $news->links('partials.general.paginate') }}
</div><!-- /.post-pagination -->
