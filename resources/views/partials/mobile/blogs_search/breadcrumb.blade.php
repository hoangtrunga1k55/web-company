<div class="BreadcrumbItem row">
    <div class="box col-12">
        <div class="box__breadcrumb">
            <span>Tin tức </span>
            <div class="icon">
                <img src="{{asset('assets/images/icon/icon_doubleArrowRight.png')}}" alt="">
            </div>
            <span class="active">{{$new->title}}</span>
        </div>
    </div>
</div>
