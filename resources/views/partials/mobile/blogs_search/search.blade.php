<div class="SearchItem row">
    <div class="box col-12">
        <div class="box__title">Tìm kiếm</div>
        <div class="box__content">
            Ex:thiết kế app, thiết kế mobile, web chuẩn seo ...
        </div>
        <div class="box__search">
            <div class="input-group">
                <form action="{{route('site.news.search')}}" method="GET">
                    <div class="input-group-prepend">
                        <button type="submit" class="input-group-text"><img src="{{ 'assets/images/icon/icon_search.png' }}" alt=""></button>
                    </div>
                    <input type="text" name="title" class="form-control" placeholder="Tìm kiếm ...">
                </form>
            </div>
        </div>
    </div>
</div>
