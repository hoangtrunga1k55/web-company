<?php
$condition['where'][] = ['slug', 'like', 'co-the-ban-thich'];
$id_items = app(\App\Repositories\Contracts\CategoryRepositoryInterface::class)->all(array('_id'), $condition)->first();
$conditions['where'][] = ['category_ids', 'all', [$id_items['_id']]];
$news = app(\App\Repositories\Contracts\NewRepositoryInterface::class)->all(array('*'), $conditions)->take(4);
?>
@if($news->count() > 0)
    <div class="MaybeYouLikeItem row">
        <div class="title col-12">
            <h1>Có thể bạn thích</h1>
            {{--        <a href="#">Tất cả</a>--}}
        </div>
        <div class="col-12">
            <div id="MaybeYouLikeBlog" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#MaybeYouLikeBlog" data-slide-to="0" class=""></li>
                    <li data-target="#MaybeYouLikeBlog" data-slide-to="1" class=""></li>
                    <li data-target="#MaybeYouLikeBlog" data-slide-to="2" class="active"></li>
                    <li data-target="#MaybeYouLikeBlog" data-slide-to="3" class=""></li>
                </ol>
                <div class="carousel-inner">
                    @foreach($news as $key => $new)
                        <div class="carousel-item {{($key == 0) ?'active':''}}">
                            <div class="box">
                                <div class="box__image">
                                    <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}" alt=""></a>
                                </div>
                                <a href="{{route('site.news.show',$new->slug)}}" class="box__title">{{$new->title}}</a>
                                <a href="{{route('site.news.show',$new->slug)}}"><div class="box__text">{{$new->description}}</div></a>
                                <div class="box__link">
                                    <a href="{{route('site.news.show',$new->slug)}}">Xem thêm </a><img src="{{ 'assets/images/icon/arrowRight.png' }}" alt="">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#MaybeYouLikeBlog" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#MaybeYouLikeBlog" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>
            </div>
        </div>
    </div>
@else
    <h3>Không tồn tại bài viết</h3>
@endif
