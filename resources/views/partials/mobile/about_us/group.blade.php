<div class="GroupItem row">
    <div class="boxOne col-12">
        <div class="boxOne__title">
            <h1>Đội ngũ và phương châm kinh doanh của chúng tôi</h1>
        </div>
        <div class="boxOne__content-one">
            <h3>PHƯƠNG CHÂM KINH DOANH</h3>
            <p class="title">
                “Lợi ích đích thực phải được kết hợp hài hòa từ lợi ích của khách
                hàng, của công ty, của mỗi nhân viên và của cộng đồng”
            </p>
            <p class="content">
                – Là một trong những công ty tiên phong trong lĩnh vực mang lại
                giải pháp toàn diện từ: quản lý bán hàng, loyalty ( tích điểm cho
                khách hàng), booking, và một số tính năng vượt trội khác chỉ bằng
                ứng dụng di động, website. Chúng tôi ý thức rằng: “Khách hàng là
                nền tảng của thành công <br>
                – Lợi ích đích thực phải được kết hợp hài hòa từ lợi ích của khách
                hàng, của công ty, của mỗi nhân viên và của cộng đồng”.<br>
                Tập thể ban lãnh đạo và nhân viên của quyết tâm sẽ hoạt động với
                phương châm: lợi ích của khách hàng là trên hết, lợi ích của người
                lao động được quan tâm, đóng góp có hiệu quả vào sự phát triển của
                cộng đồng
            </p>
        </div>
    </div>
    <div class="boxOne col-12 p-0">
        <div class="boxOne__content-two">
            <div class="text">
                HAPTECH cam kết sẽ thoả mãn tối đa lợi ích của khách hàng trên cơ
                sở cung cấp cho khách hàng những sản phẩm chất lượng, dịch vụ
                phong phú, đa dạng, đồng bộ, nhiều tiện ích, chi phí có tính cạnh
                tranh.<br>
                HAPTECH cam kết thực hiện tốt nghĩa vụ tài chính đối với ngân sách
                Nhà nước, luôn quan tâm chăm lo đến công tác xã hội, từ thiện để
                chia sẻ khó khăn của cộng đồng.
            </div>
            <div class="image">
                <img src="{{ asset('assets/images/about/pic2.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="boxTwo col-12 p-0">
        <div class="boxTwo__content-two">
            <div class="text">
                HAPTECH cam kết sẽ thoả mãn tối đa lợi ích của khách hàng trên cơ
                sở cung cấp cho khách hàng những sản phẩm chất lượng, dịch vụ
                phong phú, đa dạng, đồng bộ, nhiều tiện ích, chi phí có tính cạnh
                tranh.<br>
                HAPTECH cam kết thực hiện tốt nghĩa vụ tài chính đối với ngân sách
                Nhà nước, luôn quan tâm chăm lo đến công tác xã hội, từ thiện để
                chia sẻ khó khăn của cộng đồng.
            </div>
            <div class="image">
                <img src="{{ asset('assets/images/about/pic6.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="boxThere col-12">
        <div class="boxThere__text">
            Tất cả các cán bộ nhân viên trong công ty HAPTECH đều thấu hiểu được
            một điều đó là: “Khách hàng mới là người quyết định tương lai, sự
            tồn tại và phát triển của HAPTECH”. Vì vậy toàn thể nhân viên công
            ty HAPTECH đều luôn tâm niệm và làm việc theo suy nghĩ: “Hãy phục vụ
            khách hàng như chúng ta đang phục vụ cho chính bản thân chúng ta”.
            Khi sử dụng dịch vụ của HAPTECH, quý khách hàng sẽ cảm thấy hoàn
            toàn yên tâm và hài lòng về mọi mặt vì HAPTECH luôn cung cấp
        </div>
        <div class="boxThere__image">
            <img src="{{ asset('assets/images/about/groupMobile5.png') }}" alt="">
        </div>
    </div>
    <div class="boxFour col-12">
        <div class="boxFour__item">
            <div class="boxFour__item-number">1</div>
            <div class="boxFour__item-title">Đúng chất lượng</div>
            <div class="boxFour__item-text">
                Được đảm bảo về chất lượng dịch vụ bằng Hợp đồng có ghi rõ những
                điều khoản nêu ra: từ quy trình, tính năng, thời gian nghiệm thu
                sản phẩm như đã cam kết
            </div>
        </div>
        <div class="boxFour__item">
            <div class="boxFour__item-number">2</div>
            <div class="boxFour__item-title">Giá cả cạnh tranh</div>
            <div class="boxFour__item-text">
                Phù hợp nhất với nhu cầu và loại hình mỗi khách hàng/ doanh
                nghiệp.
            </div>
        </div>
        <div class="boxFour__item">
            <div class="boxFour__item-number">3</div>
            <div class="boxFour__item-title">Thái độ phục vụ</div>
            <div class="boxFour__item-text">
                Được HAPTECH phục vụ tận tình, chu đáo, luôn đồng hành trên từng
                chặng đường phát triển của khách hàng/ doanh nghiệp.
            </div>
        </div>
        <div class="boxFour__item">
            <div class="boxFour__item-number">4</div>
            <div class="boxFour__item-title">Dịch vụ hoàn hảo</div>
            <div class="boxFour__item-text">
                Khách hàng/ doanh nghiệp hưởng các chính sách, chế độ Dịch vụ số 1
                tại Việt Nam.
            </div>
        </div>
    </div>
</div>
