<div class="ProjectItem row">
    <div class="box col-12">
        <div class="box__title"><h1>Các dự án đã triển khai</h1></div>
        <div class="box__image">
            <img src="{{ asset('assets/images/about/pcover2.png') }}" alt="">
        </div>
        <div class="box__text">Ứng dụng Feedy</div>
        <div class="box__content">
            Ứng dụng mạng xã hội ẩm thực của Việt Nam. Nơi người dùng có thể chia sẻ công thức nấu ăn, nhà hàng,
            quán ăn ngon
        </div>
    </div>
    <div class="box col-12">
        <div class="box__title"><h1>Các dự án đã triển khai</h1></div>
        <div class="box__image">
            <img src="{{ asset('assets/images/about/pcover3.png') }}" alt="">
        </div>
        <div class="box__text">Ứng dụng hỗ trợ tiêm chủng</div>
        <div class="box__content">
            Dự án của Chính phủ về tiêm chủng vắc xin, quản lý thông tin tiêm chủng của người dân, đưa ra thông tin cần
            thiết liên quan

        </div>
    </div>
    <div class="box col-12">
        <div class="box__title"><h1>Các dự án đã triển khai</h1></div>
        <div class="box__image">
            <img src="{{ asset('assets/images/about/pcover4.png') }}" alt="">
        </div>
        <div class="box__text">Ứng dụng TANODY</div>
        <div class="box__content">
            Ứng dụngn kết bạn an toàn, bảo mật công nghệ 4.0, tìm nửa kia phù hợp qua bộ lọc chi tiết từ 1 triệu
            người dùng trong 3s
        </div>
    </div>
    <div class="box col-12">
        <div class="box__title"><h1>Các dự án đã triển khai</h1></div>
        <div class="box__image">
            <img src="{{ asset('assets/images/about/pcover1.png') }}" alt="">
        </div>
        <div class="box__text">Ứng dụng tin tức Viethome</div>
        <div class="box__content">
            Cung cấp trang thông tin,sự kiện cho cộng đồng người Việt Nam tại châu Âu
        </div>
    </div>
    <div class="box col-12">
        <div class="box__link">
            <a data-toggle="modal" data-target="#ContactNow">Liên hệ ngay </a><img src="{{ asset('assets/images/icon/arrowRight.png') }}" alt="">
        </div>
    </div>
</div>
