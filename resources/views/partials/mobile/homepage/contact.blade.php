<div class="ContactItem row">
    <div class="background">
        <img src="{{ asset('assets/images/home/contact.png') }}" alt="">
    </div>
    <div class="box col-12">
        <div class="col-lg-6 box__right">
            <form class="row" data-request="onSubmitContactForm" data-request-success="showResponse(data)" data-request-error="showError(data)">
                <div class="box__right-title col-12">
                    Đăng kí và nhận tư vấn miễn phí ngay hôm nay.<br>Nhận đãi lên
                    đên <span>30%</span>
                </div>
                <div class="box__right-title col-lg-6 pt-3">
                    <input type="text" class="form-control" name="name" placeholder="Họ tên">
                </div>
                <div class="box__right-title col-lg-6 pt-3">
                    <input type="mail" class="form-control" name="email" placeholder="Email">
                </div>
                <div class="box__right-title col-lg-6 pt-3">
                    <input type="mail" class="form-control" name="phone" placeholder="Số điện thoại">
                </div>
                <div class="box__right-title col-lg-6 pt-3">
                    <input type="text" name="business_type" class="form-control" placeholder="Lĩnh vực">

                </div>
                <div class="box__right-title col-12 pt-3">
                    <textarea class="form-control" name="message" rows="3" placeholder="Bạn muốn chúng tôi cung cấp dịch vụ gì"></textarea>
                </div>
                <div class="box__right-btn col-12 pt-3">
                    <button id="submitModal" type="submit" class="btn w-100">Gửi thông tin</button>
                </div>
            </form>
        </div>
        <div class="col-lg-6 box__left">
            <div class="box__left-image1">
                <img src="{{ asset('assets/images/home/contact1.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
