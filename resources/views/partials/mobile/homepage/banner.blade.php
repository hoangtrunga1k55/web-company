<div class="BannerItem row">
    <div class="col-12">
        <div class="content">
            <div class="content__text">
                Nhà phát triển Website và Mobile App hàng đầu Việt Nam
            </div>
            <button class="content__btn btn" data-toggle="modal" data-target="#ContactNow">
                Liên hệ với chúng tôi
            </button>
            <div class="content__image">
                <img src="{{ asset('assets/images/home/bannerHomeMobile.png') }}" alt="">
            </div>
        </div>
        <div class="background">
            <div class="background-image">
                <img src="{{ asset('assets/images/home/bannerMobile.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
