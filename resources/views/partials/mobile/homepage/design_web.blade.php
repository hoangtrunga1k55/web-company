<div class="DesignWebItem row">
    <div class="box col-12">
        <div class="row">
            <div class="box__title-gradeOne col-12">
                Khám phá các dịch vụ thiết kế <span>Website</span> chuẩn Seo
            </div>
            <div class="box__title-gradeTwo col-12">
                <span>Website</span> - Quản lý bán hàng
            </div>
            <div class="box__content-item col-12">
                <img src="{{ asset('assets/images/home/quanlybanhang1.png') }}" alt="quanlybanhang1">
            </div>
            <div class="box__content-item col-12">
                <a href=""><img src="{{ asset('assets/images/home/quanlybanhang2.png') }}" alt="quanlybanhang1"></a>
            </div>
            <div class="box__content-item col-12">
                <a href=""><img src="{{ asset('assets/images/home/quanlybanhang3.png') }}" alt="quanlybanhang1"></a>
            </div>
            <div class="box__title-gradeTwo col-12">
                <span>Website</span> - Tin tức
            </div>
            <div class="box__content-item col-12">
                <a href=""><img src="{{ asset('assets/images/home/tintuc1.png') }}" alt="quanlybanhang1"></a>
            </div>
            <div class="box__content-item col-12">
                <a href=""><img src="{{ asset('assets/images/home/tintuc2.png') }}" alt="quanlybanhang1"></a>
            </div>
            <div class="box__content-item col-12">
                <a href=""><img src="{{ asset('assets/images/home/tintuc3.png') }}" alt="quanlybanhang1"></a>
            </div>
        </div>
        <div class="box__btn">
            <a href="{{ route('site.design_web') }}" class="btn">Tham khảo thêm</a>
        </div>
    </div>
</div>
