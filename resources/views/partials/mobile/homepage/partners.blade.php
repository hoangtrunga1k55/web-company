<div class="BusinessPartnerItem row">
    <div class="box col-12">
        <div class="box__title-gradeOne">
            <p><span>99+ đối tác </span> đã tin dùng dịch vụ của chúng tôi</p>
        </div>
        <div class="box__content">
            <a href="" class="box__content-item">
                <img src="{{ asset('assets/images/home/doitac1.png') }}" alt="">
            </a>
            <a href="" class="box__content-item">
                <img src="{{ asset('assets/images/home/doitac2.png') }}" alt="">
            </a>
            <a href="" class="box__content-item">
                <img src="{{ asset('assets/images/home/doitac3.png') }}" alt="">
            </a>
            <a href="" class="box__content-item">
                <img src="{{ asset('assets/images/home/doitac4.png') }}" alt="">
            </a>
            <a href="" class="box__content-item">
                <img src="{{ asset('assets/images/home/doitac5.png') }}" alt="">
            </a>
        </div>
    </div>
</div>
