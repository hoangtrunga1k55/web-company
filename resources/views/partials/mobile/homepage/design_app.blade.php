<div class="DesignAppItem container-fluid">
    <div class="box row">
        <div class="box__title-gradeOne col-12">
            Khám phá các dịch vụ thiết kế <span>Mobile App</span>
        </div>
        <div class="box__content col-md-12">
            <div class="box__content-img">
                <img src="{{ asset('assets/images/home/app1.png') }}" alt="quanlybanhang">
            </div>
            <div class="box__content-footer">
                <div class="title"><span>App</span> - Loyalty</div>
                <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
            </div>
        </div>
        <div class="box__content col-md-12">
            <div class="box__content-img">
                <img src="{{ asset('assets/images/home/app2.png') }}" alt="quanlybanhang">
            </div>
            <div class="box__content-footer">
                <div class="title"><span>App</span> - Quản lý bán hàng</div>
                <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
            </div>
        </div>
        <div class="box__content col-md-12">
            <div class="box__content-img">
                <img src="{{ asset('assets/images/home/app3.png') }}" alt="quanlybanhang">
            </div>
            <div class="box__content-footer">
                <div class="title"><span>App</span> - Tin tức</div>
                <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
            </div>
        </div>
        <div class="box__content col-md-12">
            <div class="box__content-img">
                <img src="{{ asset('assets/images/home/app4.png') }}" alt="quanlybanhang">
            </div>
            <div class="box__content-footer">
                <div class="title"><span>App</span> - Tài chính , tiền ảo</div>
                <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
            </div>
        </div>
    </div>
</div>
