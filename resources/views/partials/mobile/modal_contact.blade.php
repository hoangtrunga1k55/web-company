<div class="modal fade" id="ContactNow" tabindex="-1" role="dialog" aria-labelledby="ContactNowTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="background">
                <img src="{{ asset('assets/images/home/bgModalContactMobile.png') }}" alt="">
            </div>
            <div class="header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="box">
                    <div class="box__title pt-3">
                        Đăng ký và nhận tư vấn miễn phí ngay hôm nay. Nhận ưu đãi
                        lên đến <span>30%</span>
                    </div>
                    <form class="box__form row" id="contact"
                          action="{{ route('site.submit-contact') }}" method="post"
                          role="form" enctype="multipart/form-data"
                          onsubmit="submitForm(this); return false;">
                        @csrf
                        <div class="col-12 pt-3">
                            <input type="text" name="name" class="form-control" placeholder="Họ tên">
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="col-6 pt-3 pr-1">
                            <input type="number" name="phone" class="form-control"
                                   placeholder="Số điện thoại">
                        </div>
                        <div class="col-6 pt-3 pl-1">
                            <input type="text" name="business_type" class="form-control"
                                   placeholder="Lĩnh vực">
                        </div>
                        <div class="col-12 pt-3">
                                <textarea class="form-control" name="message" class="form-control"
                                          rows="3" placeholder="Bạn muốn chúng tôi cung cấp dịch vụ gì"></textarea>
                        </div>
                        <div class="box__btn col-12 pt-3">
                            <button id="submitModal" type="submit" class="btn w-100 btn-submit">Gửi thông tin</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
