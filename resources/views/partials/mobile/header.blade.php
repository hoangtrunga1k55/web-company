<section class="headerMobile">
    <div class="nav">
        <a class="nav__logo" href="{{ route('site.index') }}">
            <img src="{{ asset('assets/images/home/logo.svg') }}" alt="HapLgo">
        </a>
        <div class="nav__btn">
            <input type="checkbox" id="btnToggle">
            <label for="btnToggle">
                <img src="{{ asset('assets/images/icon/icon_toggle.png') }}" alt="">
            </label>
            <div class="list">
                <div class="left"></div>
                <div class="right">
                    <div class="list__close">
                        <label for="btnToggle"><img src="{{ asset('assets/images/icon/icon_close.png') }}" alt=""></label>
                    </div>
                    <ul>
                        <li class="list__item ">
                            <a class="list__item-link" href="{{ route('site.about_us') }}">
                                Về chúng tôi
                            </a>
                        </li>
                        <li class="list__item ">
                            <a class="list__item-link" href="{{ route('site.design_web') }}">Thiết kế web</a>
                        </li>
                        <li class="list__item ">
                            <a class="list__item-link" href="{{ route('site.design_app') }}">Thiết kế app</a>
                        </li>
                        <li class="list__item ">
                            <a class="list__item-link" href="{{ route('site.news') }}">Tin tức</a>
                        </li>
                    </ul>
                    <div class="list__button">
                        <button data-toggle="modal" data-target="#ContactNow" class="btn">
                            Liên hệ ngay
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
