<div class="IntroduceItem row">
    <div class="box col-12">
        <div class="row">
            <div class="box__content col-12">
                <div class="box__content-img">
                    <img src="{{ asset('assets/images/home/app1.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Loyalty</div>
                </div>
            </div>
            <div class="box__content col-12">
                <div class="box__content-img">
                    <img src="{{ asset('assets/images/home/app2.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Quản lý bán hàng</div>
                </div>
            </div>
        </div>
    </div>
    <div class="box col-12">
        <div class="row">
            <div class="box__content col-12">
                <div class="box__content-img">
                    <img src="{{ asset('assets/images/home/app3.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Tin tức</div>
                </div>
            </div>
            <div class="box__content col-12">
                <div class="box__content-img">
                    <img src="{{ asset('assets/images/home/app4.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Tài chính , tiền ảo</div>
                </div>
            </div>
        </div>
    </div>
</div>
