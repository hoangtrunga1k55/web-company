@if($news->total() > 0)
<div class="HighlightItem row">
    @foreach($news as $key => $new)
        @if($key == 0)
    <div class="image col-7">
        <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}"
                                                              alt=""></a>
    </div>
    <div class="content col-5">
        <div class="content__category">
            <div class="content__category-text">PRODUCT</div>
            <div class="content__category-dot"></div>
            <div class="content__category-time">{{ $new->approved_at}}</div>
        </div>
        <div class="content__title">
            <a href="{{route('site.news.show',$new->slug)}}">{{$new->title}}</a>
        </div>
        <div class="content__text">
            <a href="{{route('site.news.show',$new->slug)}}">{{$new->description}}</a>
        </div>
        <div class="content__link">
            <a href="{{route('site.news.show',$new->slug)}}">Xem thêm </a><img
                src="{{ 'assets/images/icon/arrowRight.png' }}" alt="">
        </div>
    </div>
        @endif
    @endforeach
</div>
<div class="Content row">
    <div class="col-9 p-0">
        <div class="BlogListItem row">
            @foreach($news as $key => $new)
                @if($key > 0)
            <div class="col-6">
                <div class="box">
                    <div class="box__image">
                        <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}"
                                          alt=""></a>
                    </div>
                    <div class="box__content">
                        <div class="box__content-category">
                            <div class="type">PRODUCT</div>
                            <div class="dot"></div>
                            <div class="time">{{$new->created_at->format('d-M-Y')}}</div>
                        </div>
                        <div class="box__content-title">
                            <a href="{{route('site.news.show',$new->slug)}}">{{$new->title}}</a>
                        </div>
                        <div class="box__content-text">
                            <a href="{{route('site.news.show',$new->slug)}}">{!! $new->description !!}</a>
                        </div>
                        <div class="box__content-link">
                            <a href="{{route('site.news.show',$new->slug)}}">Xem thêm </a><img
                                src="{{ 'assets/images/icon/arrowRight.png' }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="col-3">
        @include('partials.web.blogs_search.hot-blog')
    </div>
</div>
@else
    <h3 style="text-align: center">Không tồn tại bài viết</h3>
@endif
<div class="post-pagination">
    {{ $news->links('partials.general.paginate') }}
</div><!-- /.post-pagination -->
