<div class="TitleItem row">
    <div class="box col-12">
        <div class="box__title">
            <h1>Tin tức</h1>
            <div class="input-group">
                <form action="/tin-tuc/tim-kiem" method="GET">
                    <div class="input-group-prepend">
                        <button type="submit" class="input-group-text"><img src="{{ 'assets/images/icon/icon_search.png' }}" alt=""></button>
                    </div>
                    <input type="text" name="title" class="form-control" placeholder="Nhập thông tin tìm kiếm">
                </form>
            </div>
        </div>
    </div>
</div>
