<?php
$condition['where'][] = ['slug', 'like', 'co-the-ban-thich'];
$id_items = app(\App\Repositories\Contracts\CategoryRepositoryInterface::class)->all(array('_id'), $condition)->first();
$conditions['where'][] = ['category_ids', 'all', [$id_items['_id']]];
$news = app(\App\Repositories\Contracts\NewRepositoryInterface::class)->all(array('*'), $conditions)->take(4);
?>
@if($news->count() > 0)
<div class="MaybeYouLikeItem row">
    <div class="title col-12">
        <h1>Có thể bạn thích</h1>
        <!--        <a href="#">Tất cả</a>-->
    </div>
    <div class="row">
        @foreach($news as $new)
        <div class="col-3">
            <div class="box">
                <div class="box__image">
                    <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}" alt=""></a>
                </div>
                <a href="{{route('site.news.show',$new->slug)}}" class="box__title">{{$new->title}}</a>
                <a href="{{route('site.news.show',$new->slug)}}"><div class="box__text">{{$new->description}}</div></a>
                <div class="box__link">
                    <a href="{{route('site.news.show',$new->slug)}}">Xem thêm </a><img src="{{ 'assets/images/icon/arrowRight.png' }}" alt="">
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@else
    <h3>Không tồn tại bài viết</h3>
@endif
