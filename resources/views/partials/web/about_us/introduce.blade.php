<div class="IntroduceItem row">
    <div class="box__one col-12">
        <div class="row">
            <div class="box__one-title col-6">
                <div>
                    Dịch chuyển số là điều tất yếu khi một loạt công ty tạo cách
                    mạng trong việc thay đổi hành vi khách hàng từ việc mua sắm, đặt
                    lịch, vận chuyển, thanh toán,.. Trong 3 năm với sự mạnh mẽ của:
                    tiki, lazada, shopee.... trong thương mại điện tử; grab, be,
                    fastgo, gojek trong di chuyển; momo, Vn pay, viettel pay,....
                    trong việc thanh toán. Thấu hiểu được điều đó, HAPTECH ra đời để
                    giúp các doanh nghiệp không chỉ đến gần với khách hàng hơn mà
                    còn nâng tầm thương hiệu trong giai đoạn chuyển dịch này.
                </div>
            </div>
            <div class="box__one-image col-6">
                <img  src="{{ asset('assets/images/about/pic3.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="box__two col-12">
        <div class="row">
            <div class="box__two-image col-6">
                <img  src="{{ asset('assets/images/about/pic4.png') }}" alt="">
            </div>
            <div class="box__two-title col-6">
                <p class="mb-0">
                    Công ty TNHH Phát Triển Công nghệ HAP GROUP đã và đang vượt qua
                    được những khó khăn ban đầu, từng bước tạo được uy tín đối với
                    khách hàng, khẳng định được khả năng cung cấp các dịch vụ phù
                    hợp với từng lĩnh vực và loại hình kinh doanh của khách hàng.
                    Chúng tôi cam kết tập trung mọi cố gắng để cung cấp các giải
                    pháp toàn diện về sản phẩm công nghệ và dịch vụ chăm sóc khách
                    hàng với chất lượng nhất, phục vụ hiệu quả nhất và đem lại nhiều
                    thành công nhất cho khách hàng. Tự tin là đơn vị luôn tiên phong
                    trong công cuộc cách mạng hóa xây dựng nền tảng vững chắc cho
                    mỗi doanh nghiệp với khách hàng qua việc cung cấp các dịch vụ
                    công nghệ và thông tin chất lượng cao bất cứ nơi đâu, bất cứ khi
                    nào.
                </p>
            </div>
        </div>
    </div>
    <div class="box__one col-12">
        <div class="row">
            <div class="box__one-title col-6">
                <div>
                    <p class="mb-0">
                        HAPTECH chuyên cung cấp các dịch vụ công nghệ - phần mềm chất
                        lượng tốt tại Hà Nội nói riêng và trên toàn quốc nói chung.
                        Với các loại hình dịch vụ đa dạng phù hợp với nhu cầu sử dụng
                        khác nhau. Lĩnh vực kinh doanh:
                    </p>
                    <ul class="pt-2">
                        <li>- Dịch vụ thiết kế và nâng cao Web chuẩn seo</li>
                        <li>- Dịch vụ Hosting</li>
                        <li>- Dịch vụ thiết kế App Mobile</li>
                        <li>- Dịch vụ cung cấp phần mềm quản lý, CRM, CSKH,...</li>
                    </ul>
                    <button class="btn">Tìm hiểu ngay</button>
                </div>
            </div>
            <div class="box__one-image col-6">
                <img  src="{{ asset('assets/images/about/pic5.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
