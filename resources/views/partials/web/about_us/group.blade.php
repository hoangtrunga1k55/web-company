<div class="GroupItem row">
    <div class="box__title col-12">
        <h1>Đội ngũ và phương châm kinh doanh của chúng tôi</h1>
    </div>
    <div class="box__content-one col-12">
        <div class="row">
            <div class="right col-3">
                <h3>PHƯƠNG CHÂM KINH DOANH</h3>
                <p>
                    “Lợi ích đích thực phải được kết hợp hài hòa từ lợi ích của
                    khách hàng, của công ty, của mỗi nhân viên và của cộng đồng”
                </p>
            </div>
            <div class="left col-9">
                <p>
                    – Là một trong những công ty tiên phong trong lĩnh vực mang lại
                    giải pháp toàn diện từ: quản lý bán hàng, loyalty ( tích điểm
                    cho khách hàng), booking, và một số tính năng vượt trội khác chỉ
                    bằng ứng dụng di động, website. Chúng tôi ý thức rằng: “Khách
                    hàng là nền tảng của thành công"
                </p>
                <p>
                    – Lợi ích đích thực phải được kết hợp hài hòa từ lợi ích của
                    khách hàng, của công ty, của mỗi nhân viên và của cộng đồng”.
                </p>
                <p>
                    – Tập thể ban lãnh đạo và nhân viên của quyết tâm sẽ hoạt động
                    với phương châm: lợi ích của khách hàng là trên hết, lợi ích của
                    người lao động được quan tâm, đóng góp có hiệu quả vào sự phát
                    triển của cộng đồng
                </p>
            </div>
        </div>
    </div>
    <div class="box__content-two col-12">
        <div class="row">
            <div class="box col-6 px-0">
                <div class="text">
                    HAPTECH cam kết sẽ thoả mãn tối đa lợi ích của khách hàng trên
                    cơ sở cung cấp cho khách hàng những sản phẩm chất lượng, dịch vụ
                    phong phú, đa dạng, đồng bộ, nhiều tiện ích, chi phí có tính
                    cạnh tranh. HAPTECH cam kết thực hiện tốt nghĩa vụ tài chính đối
                    với ngân sách Nhà nước, luôn quan tâm chăm lo đến công tác xã
                    hội, từ thiện để chia sẻ khó khăn của cộng đồng.
                </div>
                <div class="img">
                    <img
                        class="img-one"
                         src="{{ asset('assets/images/about/pic2.png') }}"
                        alt="First slide"
                    />
                    <img
                        class="img-two"
                         src="{{ asset('assets/images/about/pic1.png') }}"
                        alt="First slide"
                    />
                </div>
            </div>
            <div class="box col-6 px-0">
                <div class="text">
                    HAPTECH luôn quan tâm đến cả đời sống vật chất và đời sống tinh
                    thần của nhân viên. Mỗi nhân viên là một thành viên trong đại
                    gia đình HAPTECH, có quyền lợi và nghĩa vụ vào việc xây dựng
                    HAPTECH ngày một lớn mạnh, trở thành một tập thể đoàn kết, tương
                    trợ, văn minh, không ngừng học hỏi để hoàn thiện.
                </div>
                <div class="img">
                    <img
                        class="img-one"
                         src="{{ asset('assets/images/about/pic6.png') }}"
                        alt="First slide"
                    />
                    <img
                        class="img-two"
                         src="{{ asset('assets/images/about/pic7.png') }}"
                        alt="First slide"
                    />
                </div>
            </div>
        </div>
    </div>
    <div class="box__content-there">
        <div class="row">
            <div class="title col-12">
                Tất cả các cán bộ nhân viên trong công ty HAPTECH đều thấu hiểu
                được một điều đó là: “Khách hàng mới là người quyết định tương
                lai, sự tồn tại và phát triển của HAPTECH”. Vì vậy toàn thể nhân
                viên công ty HAPTECH đều luôn tâm niệm và làm việc theo suy nghĩ:
                “Hãy phục vụ khách hàng như chúng ta đang phục vụ cho chính bản
                thân chúng ta”. Khi sử dụng dịch vụ của HAPTECH, quý khách hàng sẽ
                cảm thấy hoàn toàn yên tâm và hài lòng về mọi mặt vì HAPTECH luôn
                cung cấp
            </div>
            <div class="img col-12">
                <img  src="{{ asset('assets/images/about/group5.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="box__footer col-12">
        <div class="row">
            <div class="box col-3">
                <div class="number">1</div>
                <div class="title">Đúng chất lượng</div>
                <div class="text">
                    Được đảm bảo về chất lượng dịch vụ bằng Hợp đồng có ghi rõ những
                    điều khoản nêu ra: từ quy trình, tính năng, thời gian nghiệm thu
                    sản phẩm như đã cam kết
                </div>
            </div>
            <div class="box col-3">
                <div class="number">2</div>
                <div class="title">Giá cả cạnh tranh</div>
                <div class="text">
                    Phù hợp nhất với nhu cầu và loại hình mỗi khách hàng/ doanh
                    nghiệp.
                </div>
            </div>
            <div class="box col-3">
                <div class="number">3</div>
                <div class="title">Thái độ phục vụ</div>
                <div class="text">
                    Được HAPTECH phục vụ tận tình, chu đáo, luôn đồng hành trên từng
                    chặng đường phát triển của khách hàng/ doanh nghiệp.
                </div>
            </div>
            <div class="box col-3">
                <div class="number">4</div>
                <div class="title">Dịch vụ hoàn hảo</div>
                <div class="text">
                    Khách hàng/ doanh nghiệp hưởng các chính sách, chế độ Dịch vụ số
                    1 tại Việt Nam.
                </div>
            </div>
        </div>
    </div>
</div>
