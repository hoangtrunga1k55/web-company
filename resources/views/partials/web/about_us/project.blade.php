<div class="ProjectItem row">
    <div class="col-6">
        <div class="box">
            <div class="box__title pr-5"><h1>Các dự án đã triển khai</h1></div>
            <div class="box__content pr-5">
                <div class="box__content-inner">
                    <div class="box__content-font">
                        <img  src="{{ asset('assets/images/about/pcover2.png') }}" alt="">
                    </div>
                    <div class="box__content-back">
                        <img  src="{{ asset('assets/images/about/pcover7.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="box__content-text pr-5">
                <h3>Ứng dụng Feedy</h3>
                <p>
                    Ứng dụng mạng xã hội ẩm thực của Việt Nam. Nơi người dùng có thể chia sẻ công thức nấu ăn, nhà hàng,
                    quán ăn ngon
                </p>
            </div>
        </div>
        <div class="box">
            <div class="box__content pr-5">
                <div class="box__content-inner">
                    <div class="box__content-font">
                        <img  src="{{ asset('assets/images/about/pcover3.png') }}" alt="">
                    </div>
                    <div class="box__content-back">
                        <img  src="{{ asset('assets/images/about/pcover6.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="box__content-text pr-5">
                <h3>Ứng dụng hỗ trợ tiêm chủng</h3>
                <p>
                    Dự án của Chính phủ về tiêm chủng vắc xin, quản lý thông tin tiêm chủng của người dân, đưa ra thông
                    tin cần thiết liên quan
                </p>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="box">
            <div class="box__content pl-5">
                <div class="box__content-inner">
                    <div class="box__content-font">
                        <img  src="{{ asset('assets/images/about/pcover4.png') }}" alt="">
                    </div>
                    <div class="box__content-back">
                        <img  src="{{ asset('assets/images/about/pcover8.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="box__content-text pl-5">
                <h3>Ứng dụng TANODY</h3>
                <p>Ứng dụngn kết bạn an toàn, bảo mật công nghệ 4.0, tìm nửa kia phù hợp qua bộ lọc chi tiết từ 1 triệu
                    người dùng trong 3s
                </p>
            </div>
        </div>
        <div class="box">
            <div class="box__content pl-5">
                <div class="box__content-inner">
                    <div class="box__content-font">
                        <img  src="{{ asset('assets/images/about/pcover1.png') }}" alt="">
                    </div>
                    <div class="box__content-back">
                        <img  src="{{ asset('assets/images/about/pcover5.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="box__content-text pl-5">
                <h3>Ứng dụng tin tức Viethome</h3>
                <p>
                    Cung cấp trang thông tin,sự kiện cho cộng đồng người Việt Nam tại châu Âu
                </p>
            </div>
            <div class="box__link pl-5">
                <a data-toggle="modal" data-target="#ContactNow">Liên hệ ngay </a><img
                 src="{{ asset('assets/images/icon/arrowRight.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
