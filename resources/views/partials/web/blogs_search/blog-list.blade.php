<div class="BlogListItem row">
    @if($news->count() > 0)
        @foreach($news as $new)
            <div class="col-12">
                <div class="box row">
                    <div class="box__image col-4">
                        <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}"
                                                                              alt=""></a>
                    </div>
                    <div class="box__content col-8">
                        <div class="box__content-category">
                            <div class="type">PRODUCT</div>
                            <div class="dot"></div>
                            <div class="time">{{$new->approved_at}}</div>
                        </div>
                        <div class="box__content-title">
                            <a href="{{route('site.news.show',$new->slug)}}">{{$new->title}}</a>
                        </div>
                        <div class="box__content-text">
                            <a href="{{route('site.news.show',$new->slug)}}">{{$new->description}}</a>
                        </div>
                        <div class="box__content-link">
                            <a href="{{route('site.news.show',$new->slug)}}">Xem thêm </a><img
                                src="{{ 'assets/images/icon/arrowRight.png' }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
            <div class="col-12 post-pagination">
                {{ $news->appends(request()->input())->links('partials.general.paginate') }}
            </div><!-- /.post-pagination -->
    @else
        <div class="notfound">
            <h2>Không tồn tại kết quả tìm kiếm</h2>
        </div>
    @endif
</div>

