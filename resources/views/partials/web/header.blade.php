<section class="headerWeb">
    <div class="header__navbar">
        <nav class="navbar navbar-expand px-0">
            <a class="navbar__logo" href="{{ route('site.index') }}">
                <img src="{{ asset('assets/images/home/logo.svg') }}" alt="HapLgo">
            </a>
            <ul class="navbar__content m-auto">
                <li class="navbar__content-item ">
                    <a class="nav-link" href="{{ route('site.about_us') }}">Về chúng tôi</a>
                </li>
                <li class="navbar__content-item ">
                    <a class="nav-link" href="{{ route('site.design_web') }}">Thiết kế web</a>
                </li>
                <li class="navbar__content-item ">
                    <a class="nav-link" href="{{ route('site.design_app') }}">Thiết kế app</a>
                </li>
                <li class="navbar__content-item ">
                    <a class="nav-link" href="{{ route('site.news') }}">Tin tức</a>
                </li>
            </ul>
            <button class="navbar__btn btn" data-toggle="modal" data-target="#ContactNow">
                <span> Liên hệ ngay</span>
            </button>
        </nav>
    </div>
</section>
