@if(isset($new))
<div class="BreadcrumbItem row">
    <div class="box col-12">
        <div class="box__breadcrumb">
            <a href="{{route('site.news',$new->slug)}}"> <span>Tin tức </span>
            </a>
            <div class="icon">
                <img src="{{ 'assets/images/icon/icon_doubleArrowRight.png' }}" alt="">
            </div>
            <span class="active">
                <a href="{{route('site.news.show',$new->slug)}}">
                    {{$new->title}}
                </a>
            </span>

        </div>
    </div>
</div>
@endif
