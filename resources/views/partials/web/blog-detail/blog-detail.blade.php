@if(isset($new))
<div class="BlogDetailItem row">
    <div class="col-12">
        <div class="box">
            <div class="box__image">
                <a href="{{route('site.news.show',$new->slug)}}"><img src="{{$new->image}}"
                                                                      alt=""></a>
            </div>
            <div class="box__content">
                <div class="box__content-category">
                    <div class="type">PRODUCT</div>
                    <div class="dot"></div>
                    <div class="time">{{ $new->created_at }}</div>
                </div>
                <div class="box__content-title">{{$new->title}}</div>
                <div class="box__content-text">  {!! $new->content !!}</div>
{{--                <div class="box__content-author">--}}
{{--                    <p>- TÁC GIẢ : {{\Illuminate\Support\Facades\Auth::user()}}</p>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</div>
@else
    <h3 style="text-align: center">Không tồn tại bài viết</h3>
@endif
