<div class="modal fade" id="ContactNow" tabindex="-1" role="dialog" aria-labelledby="ContactNowTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="background">
                <img src="{{ asset('assets/images/home/bgModalContactWeb.png') }}" alt="">
            </div>
            <div class="header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="box row">
                    <div class="box__right col-lg-7 col-md-12">
                        <div class="row m-0 p-0">
                            <div class="box__right-title col-12">
                                Đăng ký và nhận tư vấn miễn phí ngay hôm nay. Nhận ưu
                                đãi lên đến <span>30%</span>
                            </div>
                            <form class="box__form row" id="contact"
                                  action="{{ route('site.submit-contact') }}" method="post"
                                  role="form" enctype="multipart/form-data"
                                  onsubmit="submitForm(this); return false;">
                                @csrf
                                <div class="col-6 pt-3">
                                    <input type="text" class="form-control" name="name" placeholder="Họ tên">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="number" class="form-control" name="phone" placeholder="Số điện thoại">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" class="form-control" name="business_type" placeholder="Lĩnh vực">
                                </div>
                                <div class="col-12 pt-3">
                                    <textarea class="form-control" name="message" rows="4" placeholder="Bạn muốn chúng tôi cung cấp dịch vụ gì"></textarea>
                                </div>
                                <div class="box__btn col-12 pt-3">
                                    <button id="submitModal" type="submit" class="btn w-100 btn-submit">Gửi thông tin</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box__left col-5 d-none d-lg-block pt-3">
                        <img src="{{ asset('assets/images/home/iPhoneX.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
