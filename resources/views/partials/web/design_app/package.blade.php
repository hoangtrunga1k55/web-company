<div class="PackageItem row">
    <div class="col-12 title">GÓI THIẾT KẾ APP</div>
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box one">
                <div class="box__title">GÓI APP BÁN HÀNG</div>
                <div class="box__content">
                    <ul>
                        <li><i class="fas fa-check"></i> Trang chủ</li>
                        <li><i class="fas fa-check"></i>Quản lý popup</li>
                        <li><i class="fas fa-check"></i> Quản lý banner</li>
                        <li><i class="fas fa-check"></i> Chat tiếp thị khách hàng</li>
                        <li><i class="fas fa-check"></i>Giỏ hàng - Đặt hàng</li>
                        <li><i class="fas fa-check"></i>Tích hợp cổng thanh toán</li>
                        <li><i class="fas fa-check"></i>Thông báo tự động</li>
                        <li>
                            <i class="fas fa-check"></i>Định danh người dùng qua đăng
                            nhập SMS
                        </li>
                        <li class="last">
                            <i class="fas fa-minus"></i>Các tính năng nâng cao
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="box two">
                <div class="box__title">GÓI APP BÁN HÀNG THƯƠNG HIỆU</div>
                <div class="box__content">
                    <ul>
                        <li>
                            <i class="fas fa-check"></i> Tất cả tính năng của gói App
                            bán hàng
                        </li>
                        <li><i class="fas fa-check"></i>Mã giới thiệu</li>
                        <li><i class="fas fa-check"></i>Ví tích điểm</li>
                        <li><i class="fas fa-check"></i> Mã giảm giá</li>
                        <li><i class="fas fa-check"></i>Báo cáo thống kê</li>
                        <li><i class="fas fa-check"></i>Đặt lịch - Booking</li>
                        <li><i class="fas fa-check"></i>Đồng bộ kiotviet/POS</li>
                        <li>
                            <i class="fas fa-check"></i>Bản đồ tìm kiếm địa điểm gần
                            nhất
                        </li>
                        <li class="last">
                            <i class="fas fa-minus"></i>Tính năng thêm theo yêu cầu
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="box there">
                <div class="box__title">GÓI THIẾT KẾ THEO YÊU CẦU</div>
                <div class="box__content">
                    <ul>
                        <li>
                            <i class="fas fa-check"></i> Tất cả các tính năng của gói
                            App bán hàng thương hiệu
                        </li>
                        <li>
                            <i class="fas fa-check"></i>Tính năng khác báo giá theo yêu
                            cầu
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 text-center">
        <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ ngay</button>
    </div>
</div>
