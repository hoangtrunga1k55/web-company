<div class="IntroduceItem row">
    <div class="col-12 title">THIẾT KẾ APP</div>
    <div class="box col-12">
        <div class="row">
            <div class="box__content col-lg col-md-12">
                <div class="box__content-img">
                    <img  src="{{ asset('assets/images/home/app1.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Loyalty</div>
                    <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
                </div>
            </div>
            <div class="col-lg-1 p-0 m-0 d-none d-lg-block"></div>
            <div class="box__content col-lg col-md-12">
                <div class="box__content-img">
                    <img  src="{{ asset('assets/images/home/app2.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Quản lý bán hàng</div>
                    <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
                </div>
            </div>
        </div>
    </div>
    <div class="box col-12">
        <div class="row">
            <div class="box__content col-lg col-md-12">
                <div class="box__content-img">
                    <img  src="{{ asset('assets/images/home/app3.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Tin tức</div>
                    <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
                </div>
            </div>
            <div class="col-lg-1 p-0 m-0 d-none d-lg-block"></div>
            <div class="box__content col-lg col-md-12">
                <div class="box__content-img">
                    <img  src="{{ asset('assets/images/home/app4.png') }}" alt="quanlybanhang">
                </div>
                <div class="box__content-footer">
                    <div class="title"><span>App</span> - Tài chính , tiền ảo</div>
                    <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ</button>
                </div>
            </div>
        </div>
    </div>
</div>
