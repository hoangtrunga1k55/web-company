<div class="ContactItem row">
    <div class="col-12 px-0">
        <div class="box">
            <div class="box__bg">
                <img  src="{{ asset('assets/images/home/contact.png') }}" alt="">
            </div>
            <div class="row m-0">
                <div class="box__content col-6">
                    <div class="box__content-text">
                        <p>CÒN CHẦN CHỪ GÌ NỮA</p>
                        <h3>
                            Đăng ký và nhận tư vấn miễn phí ngay hôm nay. Nhận ưu đãi
                            lên đến 30%
                        </h3>
                    </div>
                    <button class="btn" data-toggle="modal" data-target="#ContactNow">Liên hệ ngay</button>
                </div>
                <div class="box__image col-6">
                    <img  src="{{ asset('assets/images/home/Laptop.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
