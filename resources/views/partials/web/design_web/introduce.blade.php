<div class="IntroduceItem row">
    <div class="col-12 title">THIẾT KẾ WEB</div>
    <div class="box col-12">
        <div class="row">
            <div class="col-4 box__introduce">
                <div class="box__introduce-title">
                    <span>Website</span> - Quản lý bán hàng
                </div>
                <div class="box__introduce-text">
                    All of these components are made in the same style, and can
                    easily be inegrated into projects, allowing you to create
                    hundreds of solutions for your future projects.
                </div>
            </div>
            <div class="box__image col-4">
                <img  src="{{ asset('assets/images/home/quanlybanhang2.png') }}" alt="">
            </div>
            <div class="box__image col-4">
                <img  src="{{ asset('assets/images/home/quanlybanhang2.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="box col-12">
        <div class="row">
            <div class="col-4 box__introduce">
                <div class="box__introduce-title">
                    <span>Website</span> - Tin tức
                </div>
                <div class="box__introduce-text">
                    All of these components are made in the same style, and can
                    easily be inegrated into projects, allowing you to create
                    hundreds of solutions for your future projects.
                </div>
            </div>
            <div class="box__image col-4">
                <img  src="{{ asset('assets/images/home/tintuc1.png') }}" alt="">
            </div>
            <div class="box__image col-4">
                <img  src="{{ asset('assets/images/home/tintuc2.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
