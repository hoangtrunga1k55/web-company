<div class="serviceItem container-fluid">
    <div class="serviceItem__title">Các dịch vụ chúng tôi cung cấp</div>
    <div class="row">
        <div class="col-lg-4 col-md-12 p-0">
            <div class="serviceItem__content">
                <div class="serviceItem__content-img one">
                    <a href="">
                        <img src="{{ asset('assets/images/home/Laptop.png') }}" alt="MacbookPro"></a>
                </div>
                <div class="serviceItem__content-text">
                    <h3>Thiết kế Website chuẩn SEO</h3>
                    <span>React , HTML5</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 p-0">
            <div class="serviceItem__content">
                <div class="serviceItem__content-img two">
                    <a href="">
                        <img src="{{ asset('assets/images/home/SamsungGalaxyS9.png') }}" alt="MacbookPro"></a>
                </div>
                <div class="serviceItem__content-text">
                    <h3>Thiết kế App Mobile độc quyền</h3>
                    <span>Android,IOS</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 p-0">
            <div class="serviceItem__content">
                <div class="serviceItem__content-img there">
                    <a href="">
                        <img src="{{ asset('assets/images/home/SamsungGalaxyS9.png') }}" alt="MacbookPro"></a>
                </div>
                <div class="serviceItem__content-text">
                    <h3>Dịch vụ Hosting nhanh chóng</h3>
                    <span>Amazon , Google Cloud</span>
                </div>
            </div>
        </div>
    </div>
</div>
