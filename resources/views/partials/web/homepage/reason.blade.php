<div class="ReasonItem container-fluid">
    <div class="box row">
        <div class="box__title-gradeOne col-12">
            Tại sao nên sử dụng dịch vụ <br>
            <span>HAP TECHNOLOGY</span>
        </div>
        <div class="col-lg-6 col-md-6">
            <a href="#" class="box__content"><div class="box__content-text">
                Giảm thiểu tối đa chi phí so với việc xây dựng đội in house
            </div>
                <div class="box__content-icon">
                    <img  src="{{ asset('assets/images/home/reason1.png') }}" alt=""></div></a>
        </div>
        <div class="col-lg-6 col-md-6">
            <a href="#" class="box__content"><div class="box__content-text">
                Tính năng tùy biến phù hợp với nghiệp vụ từng doanh nghiệp
            </div>
                <div class="box__content-icon">
                    <img  src="{{ asset('assets/images/home/reason2.png') }}" alt=""></div></a>
        </div>
        <div class="col-lg-6 col-md-6">
            <a href="#" class="box__content"><div class="box__content-text">
                Đồng hành cùng xây dựng quy trình làm việc chuẩn để đạt được
                doanh thu như mong đợi
            </div>
                <div class="box__content-icon">
                    <img  src="{{ asset('assets/images/home/reason3.png') }}" alt=""></div></a>
        </div>
        <div class="col-lg-6 col-md-6">
            <a href="#" class="box__content"><div class="box__content-text">
                Báo cáo liên tục, hàng ngày, hỗ trợ khách hàng 24/7
            </div>
                <div class="box__content-icon">
                    <img  src="{{ asset('assets/images/home/reason4.png') }}" alt=""></div></a>
        </div>
    </div>
    <div class="circle"></div>
</div>
