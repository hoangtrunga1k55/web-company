<div class="BannerItem">
    <div class="header__content">
        <div class="row">
            <div class="col-lg-8 col-md-12 left">
                <div class="left__text">
                    Nhà phát triển Website và <br> Mobile App hàng đầu Việt Nam
                </div>
                <button class="left__btn btn" data-toggle="modal" data-target="#ContactNow">
                    Liên hệ với chúng tôi
                </button>
            </div>
            <div class="col-lg-4 col-md-12 d-none d-lg-block right">
                <div class="right__image">
                    <img src="http://210.245.80.116:8833/themes/hap-technology/assets/images/home/Banner.png" alt="HapLgo">
                </div>
            </div>
        </div>
    </div>
    <div class="header__background">
        <div class="header__background-bg"></div>
        <div class="header__background-purple_circle"></div>
    </div>
</div>
