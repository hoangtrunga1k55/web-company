<?php


namespace App\Helpers;

use App\Repositories\Contracts\PermissionRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

class PermissionsHelper
{

    public static function get()
    {
        if (!Cache::tags('permissions')->has( Auth::id())) {
            $roles = Auth::user()->roles;
            $permissions_id = [];
            foreach ($roles as $role) {
                if (!is_null($role->permissions)) {
                    foreach ($role->permissions as $permission) {
                        $permissions_id[] = $permission;
                    }
                }
            }
            $permission = app(PermissionRepositoryInterface::class)
                ->findByListId($permissions_id)
                ->toArray();
            if (empty($permission)) {
                $filteredRoutes = [];
            } else {
                $filteredRoutes = array_unique(array_merge(
                    ...array_column($permission, 'routes')
                ));
            }
            Cache::tags('permissions')->add(Auth::id() , $filteredRoutes);
        }
        return Cache::tags('permissions')->get(Auth::id());
    }

    public static function can($route): bool
    {
//        return true;
        if (in_array($route, PermissionsHelper::getAllRoutes())) {
            return in_array($route, PermissionsHelper::get());
        }
        return true;
    }

    public static function getAllRoutes(){
        $routeCollection = Route::getRoutes();
        $arr = [];
        foreach ($routeCollection as $route) {
            if (in_array('permission', $route->action['middleware'] ?? [])) {
                $arr[] = $route->action['as'];
            }
        }
        return array_unique($arr);
    }

}
