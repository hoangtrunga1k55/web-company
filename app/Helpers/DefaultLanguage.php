<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class DefaultLanguage
{
    public static function getDefaultLanguage()
    {
        $languages = DB::table('languages')->get();
        foreach ($languages as $language){
            if ($language['default'] == 1){
                return $language['code'];
            }
        }
    }
}
