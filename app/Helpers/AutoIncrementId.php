<?php


namespace App\Helpers;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AutoIncrementId
{
    public static function getId($table_name)
    {
        $table = Str::singular($table_name) . '_auto_increment_id';
        $date = Carbon::now()->format("Ymd");

        $last = DB::table($table)
            ->where("date", "like", $date)
            ->get()
            ->last();

        if (is_null($last)) {
            DB::table($table)
                ->insert([
                    'date' => $date,
                    'AI_id' => 1
                ]);
            return 1;
        } else {
            DB::table($table)
                ->insert([
                    'date' => $date,
                    'AI_id' => $last['AI_id'] + 1
                ]);
            return $last['AI_id'] + 1;
        }
    }
}
