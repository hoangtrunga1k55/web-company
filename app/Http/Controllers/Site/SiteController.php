<?php

namespace App\Http\Controllers\Site;

use App\Repositories\Contracts\ConfigRepositoryInterface;
use App\Repositories\Contracts\HeaderRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Jenssegers\Agent\Agent;
use Ixudra\Curl\Facades\Curl;

class SiteController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $lang;
    public function getDefaultLanguage(){
        if (!Session::get('language')){
            Session::put('language',DefaultLanguage::getDefaultLanguage());
            return DefaultLanguage::getDefaultLanguage();
        } else{
            return  Session::get('language');
        }
    }
    public function __construct()
    {
        $config_conditions['where'][] = ['status', 'like', 1];
        $all_configs = app(ConfigRepositoryInterface::class)->all(array('*'),$config_conditions);
        $configs = [];
        foreach ($all_configs as $config) {
            $configs[$config['key']] = $config['content'];
        }
        $orderBy = array('priority' => 'asc');
        $condition = [];
        $headers = app(HeaderRepositoryInterface::class)->paginateList(6, $condition, null, $orderBy);
        $brands = DB::table('brands')->where('status', 'like', 1)->get();

        $agent = new Agent();
        $isMobile = $agent->isMobile();

        View::share('isMobile', $isMobile);
        View::share('configs', $configs);
        View::share('brands', $brands);
        View::share('headers', $headers);
    }
    public function sitemap(){
        // create new sitemap object
        $sitemap = \App::make('sitemap');

        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        $sitemap->setCache('laravel.sitemap', 60);

        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {
            // add item to the sitemap (url, date, priority, freq)
//        $sitemap->add(URL::to('/'), (string) now(), '1.0', 'daily');
            $sitemap->add(URL::to('tin-tuc'), (string) now(), '1.0', 'daily');
            // add item with translations (url, date, priority, freq, images, title, translations)
            $translations = [
//            ['language' => 'fr', 'url' => URL::to('pageFr')],
//            ['language' => 'de', 'url' => URL::to('pageDe')],
//            ['language' => 'bg', 'url' => URL::to('pageBg')],
            ];
//        $sitemap->add(URL::to('pageEn'), '2015-06-24T14:30:00+02:00', '0.9', 'monthly', [], null, $translations);

            // add item with images
            $images = [
//            ['url' => URL::to('images/pic1.jpg'), 'title' => 'Image title', 'caption' => 'Image caption', 'geo_location' => 'Plovdiv, Bulgaria'],
//            ['url' => URL::to('images/pic2.jpg'), 'title' => 'Image title2', 'caption' => 'Image caption2'],
//            ['url' => URL::to('images/pic3.jpg'), 'title' => 'Image title3'],
            ];
//        $sitemap->add(URL::to('post-with-images'), '2015-06-24T14:30:00+02:00', '0.9', 'monthly', $images);

            // get all news from db
            $news_list = DB::table('new')
                ->where('status', "like", News::RELEASE)
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            // add every news to the sitemap
            foreach ($news_list as $news) {
                $sitemap->add(URL::to('tin-tuc/'. $news['slug']), (string) now(), "1.0", "daily");
                $sitemap->add(URL::to('news/'. $news['slug']), (string) now(), "1.0", "daily");
            }

            // get all category from db
            $categories = DB::table('category')
                ->where('status', "like", "1")
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            // add every category to the sitemap
            foreach ($categories as $category) {
                $sitemap->add(URL::to('danh-muc/'. $category['slug']), (string) now(), "1.0", "daily");
                $sitemap->add(URL::to('category/'. $category['slug']), (string) now(), "1.0", "daily");
            }
            // get all tag from db
            $tags = DB::table('tags')
                ->where('status', "like", 1)
                ->orderBy('created_at', 'desc')
                ->whereNull('deleted_at')
                ->get();
            // add every tag to the sitemap
            foreach ($tags as $tag) {
                $sitemap->add(URL::to('tag/'. $tag['slug']), (string) now(), "1.0", "daily");
            }
        }

        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');
    }

    public function changeLanguageWeb($language)
    {
        \Illuminate\Support\Facades\Session::put('language', $language);
        $key = 'config_languages';
        Cache::tags('query')->flush($key);
        if (!Cache::tags('query')->has($key)){
            $response = Curl::to('http://127.0.0.1:8001/api/config_language/get-language')
                ->withData( array( 'lang' => $language ) )
                ->post();
            $response = Cache::tags('query')->put($key,$response,now()->addMinutes(10));
        }
        return redirect()->back();
    }

}
