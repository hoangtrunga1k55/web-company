<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\LanguageRequest;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class LanguageController extends Controller
{

    protected $languageRepository;

    public function __construct(LanguageRepositoryInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function changeLanguageWeb($language)
    {
        \Illuminate\Support\Facades\Session::put('language', $language);
        return redirect()->back();
    }
}
