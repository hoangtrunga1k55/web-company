<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\HeaderRepositoryInterface;
use App\Repositories\Contracts\NewRepositoryInterface;
use App\Repositories\Contracts\PackageRepositoryInterface;
use App\Repositories\Contracts\TagRepositoryInterface;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;

class BlogController extends SiteController
{
    protected $newRepository;
    protected $categoryRepository;
    protected $tagRepository;
    protected $packageRepository;
    protected $headerRepository;

    public function __construct(
        NewRepositoryInterface $newRepository,
        CategoryRepositoryInterface $categoryRepository,
        PackageRepositoryInterface $packageRepository,
        TagRepositoryInterface $tagRepository,
        HeaderRepositoryInterface $headerRepository
    )
    {
        parent::__construct();
        $this->newRepository = $newRepository;
        $this->categoryRepository = $categoryRepository;
        $this->packageRepository = $packageRepository;
        $this->tagRepository = $tagRepository;
        $this->headerRepository = $headerRepository;
    }


    public function index()
    {
        $orderBy = array('priority' => 'asc');
        $condition = [];
        $headers = $this->headerRepository->paginateList(6, $condition, null, $orderBy);
        $packages = $this->packageRepository->paginateList(3, null, null, $orderBy);
        return view('pages.homepage', compact('packages', 'headers'));
    }

    public function getBlogSearch(Request $request){
        $conditions = [];
        $orderBy = null;
        if (isset($request->title)) {
            $conditions['where'][] = ['title', 'like', "%" . $request->title . "%"];
            $orderBy = array('approved_at' => 'desc');
        }
        $news = $this->newRepository->paginateList(2, $conditions, null, $orderBy);
        return view('pages.blog_search',compact('news'));
    }

    public function aboutUs()
    {
        return view('pages.about');
    }
    public function designApp()
    {
        return view('pages.design_app');
    }
    public function designWeb()
    {
        return view('pages.design_web');
    }


    public function getNewsByCategory($slug)
    {
        return $this->getNew($slug, 'category');
    }

    public function getNewsByTag($slug)
    {
        return $this->getNew($slug, 'tag');
    }

    public function getNew($slug, $by)
    {
        $condition = [];
        $conditionFindBySlug['where'][] = ['slug', 'like', $slug];
        $categories = $this->getCategoryAtribute();
        $tags = $this->getTagAtribute();
        $orderBy = array('priority' => 'asc');
        $headers = $this->headerRepository->paginateList(6, $condition, null, $orderBy);
        if ($by == 'tag') {
            $items = $this->tagRepository->findBy('slug', $slug);
            $id_items = $this->tagRepository->all(array('_id'), $conditionFindBySlug)->first();
            $category = null;
        } else {
            $items = $this->categoryRepository->findBy('slug', $slug);
            $id_items = $this->categoryRepository->all(array('_id'), $conditionFindBySlug)->first();
            $category = $this->categoryRepository->findBy('slug', $slug);
        }

        $conditions['where'][] = ['category_ids', 'all', [$id_items['_id']]];
        $conditions['where'][] = ['status', 'like', News::RELEASE];
        $news = $this->newRepository->paginateList(10, $conditions);

        return view('site.pages.blog', compact(
            'news',
            'categories',
            'category',
            'items',
            'tags',
            'headers',
            'slug',
        ));
    }

    public function getCategoryAtribute()
    {
        $conditionCategory['where'][] = ['status', 'like', Category::STATUS];
        return $this->categoryRepository->all(array('*'), $conditionCategory)->take(10);
    }

    public function getTagAtribute()
    {
        $conditionTag['where'][] = ['status', 'like', Tag::STATUS];
        $tags = $this->tagRepository->all(array('*'), $conditionTag)->take(10);
        return $tags;
    }

    public function getBlog(Request $request)
    {
        $conditions = [];
        if (isset($request->title)) {
            $conditions['where'][] = ['title', 'like', "%" . $request->title . "%"];
        }
        $conditions ['where'][] = ['status', 'like', News::RELEASE];
        $conditions ['where'][] = ['created_at', "<=", now()];
        $orderBy = array('priority' => 'asc');
        $headers = $this->headerRepository->paginateList(6,null, null, $orderBy);
        $news = $this->newRepository->paginateList(5, $conditions, null, $orderBy);
        $categories = $this->getCategoryAtribute();
        $tags = $this->getTagAtribute();

        return view('pages.blog', compact('news', 'categories', 'tags', 'headers'));
    }

    public function getBlogDetails($slug)
    {
        $new = $this->newRepository->findBy('slug', $slug);
        $orderBy = array('priority' => 'asc');
        $headers = $this->headerRepository->paginateList(6, null, null, $orderBy);
        $categories = $this->getCategoryAtribute();
        $tags = $this->getTagAtribute();
        return view('pages.blog_detail', compact(
            'new',
            'categories',
            'tags',
            'headers',
            'slug'
        ));
    }
}
