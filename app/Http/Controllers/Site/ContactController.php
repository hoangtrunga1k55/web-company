<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Repositories\Contracts\ContactRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ContactController extends SiteController
{
    protected $contactRepository;

    public function __construct(ContactRepositoryInterface $contactRepository)
    {
        parent::__construct();
        $this->contactRepository = $contactRepository;
    }

    public function store(ContactRequest $request)
    {
        $attributes = $request->validated();
        $attributes['message'] = strip_tags($attributes["message"]);
        if (isset($attributes['theme'])){
            $attributes['message'] = "Mua giao diện {$attributes['theme']} \n" . $attributes['message'];
        }
        $this->contactRepository->create($attributes);
        $this->sendMessage($attributes);

        return response()->json([
            'status' => 'success',
            'message' => 'Chúng tôi đã tiếp nhận tin nhắn của bạn'
        ]);
    }

    public function sendMessage($attributes)
    {
        $bot_token = "1784190800:AAEfqs0gs0QDI1rj-uQPt6Oa5b7NJXqk3To";
        $chat_id = "-1001180387496";

        $content = urlencode(
            "Họ tên: {$attributes["name"]}\n" .
            "Số điện thoại: {$attributes["phone"]}\n" .
            "Email: {$attributes["email"]}\n" .
            "Lĩnh vực: {$attributes["business_type"]}\n" .
            "Nội dung: \n".
            $attributes["message"]
        );
        Http::get("https://api.telegram.org/bot{$bot_token}/sendMessage?text={$content}&chat_id={$chat_id}");
    }
}
