<?php

namespace App\Models;

class Vendor extends MongoModel
{
    //
    protected $collection = 'vendors';
    protected $guarded = [];


    public function news()
    {
        return $this->belongsToMany(News::class);
    }
}
