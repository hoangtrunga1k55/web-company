<?php

namespace App\Models;

class Brand extends MongoModel
{
    protected $collection = 'brands';
    protected $guarded = [];
}
