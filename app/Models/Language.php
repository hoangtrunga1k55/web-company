<?php

namespace App\Models;

class Language extends MongoModel
{
    protected $collection = 'languages';

    protected $guarded = [];

}
