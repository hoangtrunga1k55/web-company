<?php

namespace App\Models;

class Config extends MongoModel
{
    protected $collection = 'configs';

    protected $guarded = [];

    function getID(): string
    {
        return isset($this->_id) ? $this->_id : "";
    }
}
