<?php

namespace App\Models;

class Package extends MongoModel
{
    protected $collection = 'packages';
    protected $guarded = [];
}
