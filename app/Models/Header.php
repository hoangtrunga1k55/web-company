<?php

namespace App\Models;

class Header extends MongoModel
{
    protected $collection = 'headers';

    protected $guarded = [];

    function getID(): string
    {
        return isset($this->_id) ? $this->_id : "";
    }
}
