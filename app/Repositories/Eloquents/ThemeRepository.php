<?php

namespace App\Repositories\Eloquents;

use App\Models\Theme;
use App\Repositories\Contracts\ThemeRepositoryInterface;

class ThemeRepository extends BaseRepository implements ThemeRepositoryInterface
{
    function __construct(Theme $model)
    {
        $this->model = $model;
    }
}
