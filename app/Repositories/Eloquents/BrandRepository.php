<?php

namespace App\Repositories\Eloquents;

use App\Models\Brand;
use App\Repositories\Contracts\BrandRepositoryInterface;

class BrandRepository extends BaseRepository implements BrandRepositoryInterface
{
    function __construct(Brand $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Brand::class;
    }
}
