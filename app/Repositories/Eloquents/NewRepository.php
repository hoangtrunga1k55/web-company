<?php

namespace App\Repositories\Eloquents;

use App\Models\Category;
use App\Models\News;
use App\Repositories\Contracts\NewRepositoryInterface;

class NewRepository extends BaseRepository implements NewRepositoryInterface
{
    function __construct(News $model)
    {
        $this->model = $model;
    }
}
