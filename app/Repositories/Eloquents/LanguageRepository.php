<?php

namespace App\Repositories\Eloquents;


use App\Models\Language;
use App\Repositories\Contracts\LanguageRepositoryInterface;

class LanguageRepository extends BaseRepository implements LanguageRepositoryInterface
{
    function __construct(Language $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Language::class;
    }
}
