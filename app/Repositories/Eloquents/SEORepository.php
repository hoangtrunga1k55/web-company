<?php

namespace App\Repositories\Eloquents;

use App\Models\SEO;
use App\Repositories\Contracts\SEORepositoryInterface;

class SEORepository extends BaseRepository implements SEORepositoryInterface
{
    function __construct(SEO $model)
    {
        $this->model = $model;
    }
}
