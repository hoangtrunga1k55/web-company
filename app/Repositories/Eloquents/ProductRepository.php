<?php

namespace App\Repositories\Eloquents;

use App\Models\Product;
use App\Repositories\Contracts\ProductRepositoryInterface;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    function __construct(Product $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Product::class;
    }
}
