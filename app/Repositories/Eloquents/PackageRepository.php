<?php

namespace App\Repositories\Eloquents;

use App\Models\Banner;
use App\Models\Package;
use App\Repositories\Contracts\PackageRepositoryInterface;

class PackageRepository extends BaseRepository implements PackageRepositoryInterface
{
    function __construct(Package $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Package::class;
    }
}
