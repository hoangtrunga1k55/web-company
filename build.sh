#!/usr/bin/env bash
#!/bin/bash
IMAGE="registry.gitlab.com/hap-tech/web-company"

#######   Processing  ###########
docker build -t $IMAGE .
docker push $IMAGE
#######   Processing  ###########

#get current hash and see if it already has a tag
echo "###############################################################"
